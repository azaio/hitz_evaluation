########################################################################################################################################################

# Main evaluation framework for HitZ

########################################################################################################################################################

# Required packages

import h5py
import numpy as np
import matplotlib.pyplot as plt
import itertools
from matplotlib import colors
import glob
import mplhep as hep
hep.style.use(hep.style.ATLAS)

print("\n=========================== \n" + " Calculation of ROC curves \n" + "=========================== \n")

########################################################################################################################################################

# Loading data

base_path = '/eos/user/b/backes/QT/preprocessing/'
network_base = '/eos/user/b/backes/QT/salt/salt/logs/' + "Hitz_new_pp_large_lr_200_20240312-T090531" + "/ckpts/epoch=*." 

test_path_1 = glob.glob(base_path + "Dipz_jfex/user" + ".*.h5/*.h5")[0]
network_path_1 = glob.glob(network_base + "37826109" + ".*.h5")[0]


num_jets = -2 #10000

with h5py.File(test_path_1, 'r') as test_f:
    jets_sig = test_f['jets'][:num_jets]
    pt_MeV_sig = jets_sig['pt']
    pt_sig = pt_MeV_sig/1000
    eventNumber_sig = jets_sig['eventNumber']
    mc_weight_sig = jets_sig['mcEventWeight']
    TruthZ_sig = jets_sig['TruthJetPVz']
    HadronConeExclTruthLabelID_sig = jets_sig['HadronConeExclTruthLabelID']
    bs_z = jets_sig['primaryVertexBeamspotZ']
    bs_z_sigma = jets_sig['primaryVertexDetectorZUncertainty']
with h5py.File(network_path_1, 'r') as f:
    jets = f['jets'][:num_jets]
    jet_z_pred_sig = jets["gaussian_regression_TruthJetPVz"]
    jet_z_stddev_sig = jets["gaussian_regression_TruthJetPVz_stddev"]



########################################################################################################################################################

# Include 4 jet condition for signal

# count_throw_away = 0
# sum_counter = 0 
# for id in np.unique(eventNumber_sig):
#     _, counts = np.unique(TruthZ_sig[eventNumber_sig == id], return_counts=True)
#     if len(eventNumber_sig[eventNumber_sig == id]) < 4:
#         sum_counter += 1
#     if max(counts) < 4 :
#         mc_weight_sig[eventNumber_sig == id] = 0.
#         count_throw_away += 1

# print("4-jet-condition done. Threw away {:0.2f}% of the test data.".format(100*sum_counter/len(np.unique(eventNumber_sig))))
# print("4-jet-condition from one vertex done. Threw away {:0.2f}% of the test data.".format(100*count_throw_away/len(np.unique(eventNumber_sig))))


########################################################################################################################################################

# Include 4 bjet condition for signal

# count = 0
# sum_counter = 0
# for id in np.unique(eventNumber_sig):
#     jets = HadronConeExclTruthLabelID_sig[eventNumber_sig == id]
#     bjets = jets[jets == 5]
#     if len(eventNumber_sig[eventNumber_sig == id]) < 4:
#         sum_counter += 1
#     if len(bjets) < 4 :
#         mc_weight_sig[eventNumber_sig == id] = 0.
#         count += 1

# print("4-jet-condition done. Threw away {:0.2f}% of the test data".format(100*sum_counter/len(np.unique(eventNumber_sig))))
# print("4-b-jet-condition done. Threw away {:0.2f}% of the test data.".format(100*count/len(np.unique(eventNumber_sig))))


########################################################################################################################################################

# Implementation of MLPL calculation

def calculate_MLPL(ev_num, n, m, cur_eN, cur_pt, cur_pred, cur_std):

    # Get m leading pt jets 
    this_event_pt = cur_pt[cur_eN==ev_num].argsort()
    pred = cur_pred[cur_eN==ev_num]
    std = cur_std[cur_eN==ev_num]
    pred = pred[this_event_pt[::-1]]
    pred = pred[:m]
    std = std[this_event_pt[::-1]]
    std = std[:m]

    # Calculate MLPL(n,m) for each n-jet combinations out of the m jets
    L_max = -9999999999.
    z_MLPL = 0.
    sig_MLPL = 0.

    for subset in itertools.combinations(range(m), n):
        log_l = -0.5*np.log(2*np.pi)*n
        log_l += -np.sum(np.log(std[[subset]]), dtype=float)
        term1 = np.sum(np.divide(pred[[subset]], std[[subset]]**2, dtype=float), dtype=float)
        term2 = np.sum(np.divide(np.ones_like(std[[subset]],dtype=float), std[[subset]]**2, dtype=float), dtype=float)
        log_l += -np.sum( np.divide((term1/term2 - pred[[subset]])**2,(2.*std[[subset]]**2), dtype=float))
        if log_l > L_max:
            L_max = log_l
            z_MLPL = term1/term2
            sig_MLPL = np.sqrt(1./term2)

    return z_MLPL, sig_MLPL


n = 4
m = 6

z_MLPL_arr = np.array([])
sig_MLPL_arr = np.array([])
z_bs_arr = np.array([])
z_bs_sig_arr = np.array([])
truth_z_arr = np.array([])
mc_weight_arr = np.array([])


for ev_num in np.unique(eventNumber_sig):
    jets = eventNumber_sig[eventNumber_sig==ev_num].shape[0]
    if jets >= n:

        if m == "All":
            z_MLPL, sig_MLPL = calculate_MLPL(ev_num, n, jets, eventNumber_sig, pt_sig, jet_z_pred_sig, jet_z_stddev_sig)
        else:
            if jets >= m: 
                z_MLPL, sig_MLPL = calculate_MLPL(ev_num, n, m, eventNumber_sig, pt_sig, jet_z_pred_sig, jet_z_stddev_sig)
            else:
                z_MLPL, sig_MLPL = calculate_MLPL(ev_num, n, jets, eventNumber_sig, pt_sig, jet_z_pred_sig, jet_z_stddev_sig)

        z_MLPL_arr = np.append(z_MLPL_arr, z_MLPL)
        sig_MLPL_arr = np.append(sig_MLPL_arr, sig_MLPL)
        z_bs_arr = np.append(z_bs_arr, bs_z[eventNumber_sig==ev_num][0])
        z_bs_sig_arr = np.append(z_bs_sig_arr, bs_z_sigma[eventNumber_sig==ev_num][0])
        mc_weight_arr = np.append(mc_weight_arr, mc_weight_sig[eventNumber_sig==ev_num][0])

        values, counts = np.unique(TruthZ_sig[eventNumber_sig == ev_num], return_counts=True)
        truth_z_value = values[np.argmax(counts)]

        truth_z_arr = np.append(truth_z_arr, truth_z_value)


########################################################################################################################################################

# 2D plot

bin_min = 0
bin_max = 3

plt.figure(figsize=(10,8))
h = plt.hist2d(np.abs(z_MLPL_arr-truth_z_arr)/sig_MLPL_arr, np.abs(z_bs_arr-truth_z_arr)/z_bs_sig_arr, bins=30, range=[[bin_min,bin_max],[bin_min,bin_max]])#, norm=colors.LogNorm())
cb = plt.colorbar(h[3])
cb.ax.tick_params(labelsize=20)
plt.xlabel("$|z_{MLPL} - z_t|/\\sigma_{MLPL}$ [mm]", fontsize=20)
plt.ylabel("$|z_{beamspot} - z_t|/\\sigma_{beamspot}$ [mm]", fontsize=20)
plt.xlim(bin_min, bin_max)
plt.ylim(bin_min, bin_max)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
x = [bin_min,bin_max]
plt.plot(x,x,c='red')
hep.atlas.label(loc=0, data=True, label="Work in Progress", rlabel='')
plt.savefig("bs_comparison/2D_plot.png", format='png', bbox_inches='tight')
plt.close()


############################################################################

# Individual performance

X_max = [1,2,3,5]

# jet_diff = truth_z_arr-z_MLPL_arr
# jet_z_std = sig_MLPL_arr

jet_diff = truth_z_arr-z_bs_arr
jet_z_std = z_bs_sig_arr

bin_number = 40



def calculate_efficiency_X(X, variable, bin_min, bin_max):
    
    N1, bin_edges = np.histogram(variable, bins=bin_number, range=(bin_min,bin_max))
    N3, bin_edges = np.histogram(variable[np.abs(jet_diff)<X*jet_z_std], bins=bin_number, range=(bin_min,bin_max))

    bins = (bin_edges[1:]+bin_edges[:bin_number])/2
    ratio = np.divide(N3, N1, out=np.zeros_like(N3, dtype=float), where=N1!=0, casting='unsafe')
    event_fraction = np.shape(variable[np.abs(jet_diff)<X*jet_z_std])[0]/np.shape(variable)[0]

    return ratio, bins, event_fraction


def calculate_efficiency_list_X(X_max, variable, bin_min, bin_max):
    
    r = []
    ev_frac = []

    for X in X_max:
        ratio, bins, event_fraction = calculate_efficiency_X(X, variable, bin_min, bin_max)
        r.append(ratio)
        ev_frac.append(event_fraction)

    return  r, bins, ev_frac


# def efficiency_plot_X(X_max, variable, bin_min, bin_max, label):

#     r,  bins, ev_frac = calculate_efficiency_list_X(X_max, variable, bin_min, bin_max)

#     color = iter(plt.cm.rainbow(np.linspace(0, 1, len(X_max))))
#     plt.figure(figsize=(12,8))
#     for i, ratio in enumerate(r):
#         c = next(color)
#         plt.step(bins, ratio, color=c, label = '{:0.1f}$\%$ of Events with $|z_t-z_M|<${}$\\sigma_M$'.format(ev_frac[i]*100, X_max[i]), where='mid')
#     plt.legend(fontsize=20, frameon=False)
#     plt.xlabel(label, fontsize=20)
#     plt.ylabel("$|z_t-z_{MLPL}|<X\\sigma_{MLPL}$ / All Events", fontsize=20)
#     plt.ylim(0,1.1)
#     plt.xticks(fontsize=20)
#     plt.yticks(fontsize=20)
#     hep.atlas.label(loc=0, data=True, label="Work in Progress", rlabel='')
#     plt.savefig("bs_comparison/" + label + "X_sigma_Efficiency.png", format='png', bbox_inches='tight')
#     plt.close()

# efficiency_plot_X(X_max, jet_z_std, 0, 40, "$\sigma_{MLPL}$")


def efficiency_plot_X(X_max, variable, bin_min, bin_max, label):

    r,  bins, ev_frac = calculate_efficiency_list_X(X_max, variable, bin_min, bin_max)

    color = iter(plt.cm.rainbow(np.linspace(0, 1, len(X_max))))
    plt.figure(figsize=(12,8))
    for i, ratio in enumerate(r):
        c = next(color)
        plt.step(bins, ratio, color=c, label = '{:0.1f}$\%$ of Events with $|z_t-z_B|<${}$\\sigma_B$'.format(ev_frac[i]*100, X_max[i]), where='mid')
    plt.legend(fontsize=20, frameon=False)
    plt.xlabel(label, fontsize=20)
    plt.ylabel("$|z_t-z_{beamspot}|<X\\sigma_{beamspot}$ / All Events", fontsize=20)
    plt.ylim(0,1.1)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    hep.atlas.label(loc=0, data=True, label="Work in Progress", rlabel='')
    plt.savefig("bs_comparison/" + label + "X_sigma_Efficiency.png", format='png', bbox_inches='tight')
    plt.close()


efficiency_plot_X(X_max, jet_z_std, 0, 0.3, "$\sigma_{beamspot}$")


########################################################################################################################################################

print("Evaluation successful.")