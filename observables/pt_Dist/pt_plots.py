############################################################################

# Visualize pt distributions of input datasets

############################################################################




# Required packages

import h5py
import numpy as np
import math
from puma import Histogram, HistogramPlot, Roc, RocPlot
from puma.metrics import calc_rej
from puma.utils import get_good_colours, get_good_linestyles, logger
import matplotlib.pyplot as plt
from matplotlib import colors
import itertools
import glob

############################################################################


# Loading Data


# path1 = '/eos/user/b/backes/QT/preprocessing/mo_files/user.backes.'

# path2 = '.e8514_e8528_s4159_s4114_r14774_r14754.tdd.trigger_emtopo_data.24_0_16.r12684_r12782.v1-208-gc25c7c3_output.h5/vds/vds.h5'

# test_path_2 = path1 + '801165' + path2 

# test_path_3 = path1 + '801166' + path2 

# test_path_4 = path1 + '801167' + path2 

# test_path_5 = path1 + '801168' + path2 

# test_path_6 = path1 + '801169' + path2

# reference = "Mo"



path1 = '/eos/user/b/backes/QT/preprocessing/' + 'L1_jfex' + '/user.backes.'

path2 = '.*.h5/*.h5'

# test_path_1 = glob.glob(path1 + '601229' + path2)[0]
test_path_1 = glob.glob(path1 + '601479' + path2)[0]

test_path_2 = glob.glob(path1 + '801165' + path2)[0]

test_path_3 = glob.glob(path1 + '801166' + path2)[0]

test_path_4 = glob.glob(path1 + '801167' + path2)[0]

test_path_5 = glob.glob(path1 + '801168' + path2)[0]

test_path_6 = glob.glob(path1 + '801169' + '.*.h5/*_000001.output.h5')[0]

reference = "Mathias"


print("\n=================== \n" + "  " + reference + " pt plots \n" + "=================== \n")

############################################################################

# Load test data

# Signal

with h5py.File(test_path_1, 'r') as test_f:
    jets_sig = test_f['jets']
    pt_MeV_sig = jets_sig['pt']
    pt_sig = pt_MeV_sig/1000
    eta_sig = jets_sig['eta']
    eventNumber_sig = jets_sig['eventNumber']
    mc_weight_sig = jets_sig['mcEventWeight']
    # deltapt_sig = jets_sig['deltaPtToTruthJet']

# Background JZ slices

with h5py.File(test_path_2, 'r') as test_f:
    jets_jz0 = test_f['jets']
    pt_MeV_jz0 = jets_jz0['pt']
    pt_jz0 = pt_MeV_jz0/1000
    eta_jz0 = jets_jz0['eta']
    eventNumber_jz0 = jets_jz0['eventNumber']
    mc_weight_jz0 = jets_jz0['mcEventWeight']
    # deltapt_jz0 = jets_jz0['deltaPtToTruthJet']

# mc_weight_jz0[mc_weight_jz0!=1.] = 0.

with h5py.File(test_path_3, 'r') as test_f:
    jets_jz1 = test_f['jets']
    pt_MeV_jz1 = jets_jz1['pt']
    pt_jz1 = pt_MeV_jz1/1000
    eta_jz1 = jets_jz1['eta']
    eventNumber_jz1 = jets_jz1['eventNumber']
    mc_weight_jz1 = jets_jz1['mcEventWeight']
    # deltapt_jz1 = jets_jz1['deltaPtToTruthJet']

with h5py.File(test_path_4, 'r') as test_f:
    jets_jz2 = test_f['jets']
    pt_MeV_jz2 = jets_jz2['pt']
    pt_jz2 = pt_MeV_jz2/1000
    eta_jz2 = jets_jz2['eta']
    eventNumber_jz2 = jets_jz2['eventNumber']
    mc_weight_jz2 = jets_jz2['mcEventWeight']
    # deltapt_jz2 = jets_jz2['deltaPtToTruthJet']

with h5py.File(test_path_5, 'r') as test_f:
    jets_jz3 = test_f['jets']
    pt_MeV_jz3 = jets_jz3['pt']
    pt_jz3 = pt_MeV_jz3/1000
    eta_jz3 = jets_jz3['eta']
    eventNumber_jz3 = jets_jz3['eventNumber']
    mc_weight_jz3 = jets_jz3['mcEventWeight']
    # deltapt_jz3 = jets_jz3['deltaPtToTruthJet']

with h5py.File(test_path_6, 'r') as test_f:
    jets_jz4 = test_f['jets']
    pt_MeV_jz4 = jets_jz4['pt']
    pt_jz4 = pt_MeV_jz4/1000
    eta_jz4 = jets_jz4['eta']
    eventNumber_jz4 = jets_jz4['eventNumber']
    mc_weight_jz4 = jets_jz4['mcEventWeight']
    # deltapt_jz4 = jets_jz4['deltaPtToTruthJet']


############################################################################

# MC reweighting parameters

cs_jz0 = 78580000000.0 *1e-11  #0.7651165653000001 
filter_jz0 = 9.736785E-01 
cs_jz1 = 93901000000.0  *1e-11 #0.0032993956809600007 
filter_jz1 = 3.513696E-02
cs_jz2 = 2582600000.0  *1e-11#2.6009622717000003e-05 
filter_jz2 = 1.006522E-02 
cs_jz3 = 28528000.0  *1e-11  #3.3590422200000005e-07 
filter_jz3 = 1.190844E-02 
cs_jz4 = 280140.0  *1e-11 #3.8673973868e-09
filter_jz4 = 1.375122E-02


total_events_jz0 = np.unique(eventNumber_jz0).shape[0]
total_events_jz1 = np.unique(eventNumber_jz1).shape[0]
total_events_jz2 = np.unique(eventNumber_jz2).shape[0]
total_events_jz3 = np.unique(eventNumber_jz3).shape[0]
total_events_jz4 = np.unique(eventNumber_jz4).shape[0]

print("Number of JZ0 events: ", total_events_jz0)
print("Number of JZ1 events: ", total_events_jz1)
print("Number of JZ2 events: ", total_events_jz2)
print("Number of JZ3 events: ", total_events_jz3)
print("Number of JZ4 events: ", total_events_jz4)


# if weighted with 1/sum(weights)

_, unique_events_jz0 = np.unique(eventNumber_jz0, return_index=True)
total_events_jz0 = np.sum(mc_weight_jz0[unique_events_jz0])
_, unique_events_jz1 = np.unique(eventNumber_jz1, return_index=True)
total_events_jz1 = np.sum(mc_weight_jz1[unique_events_jz1])
_, unique_events_jz2 = np.unique(eventNumber_jz2, return_index=True)
total_events_jz2 = np.sum(mc_weight_jz2[unique_events_jz2])
_, unique_events_jz3 = np.unique(eventNumber_jz3, return_index=True)
total_events_jz3 = np.sum(mc_weight_jz3[unique_events_jz3])
_, unique_events_jz4 = np.unique(eventNumber_jz4, return_index=True)
total_events_jz4 = np.sum(mc_weight_jz4[unique_events_jz4])


weight_jz0 = cs_jz0 * filter_jz0 / total_events_jz0 *50000
weight_jz1 = cs_jz1 * filter_jz1 / total_events_jz1 *50000
weight_jz2 = cs_jz2 * filter_jz2 / total_events_jz2 *50000
weight_jz3 = cs_jz3 * filter_jz3 / total_events_jz3 *50000
weight_jz4 = cs_jz4 * filter_jz4 / total_events_jz4 *50000

############################################################################

# Potential spike cut (not working properly)

# n_jets = 0
# n_nan = 0
# for ev_num in np.unique(eventNumber_jz0):

#     for i in range(eventNumber_jz0[eventNumber_jz0==ev_num].shape[0]):
#         n_jets +=1
#         if np.isnan(deltapt_jz0[eventNumber_jz0==ev_num][i]):
#             n_nan +=1

# print(n_nan/n_jets)

# for ev_num in np.unique(eventNumber_jz0):
    # if eventNumber_jz0[eventNumber_jz0==ev_num].shape[0]>1:
    #     this_event_pt = pt_jz0[eventNumber_jz0==ev_num].argsort()
    #     cur_pt = pt_jz0[eventNumber_jz0==ev_num]
    #     cur_pt = cur_pt[this_event_pt[::-1]]
    #     cur_deltapt = deltapt_jz0[eventNumber_jz0==ev_num]
    #     cur_deltapt = cur_deltapt[this_event_pt[::-1]]
        
    #     if np.abs(cur_pt[0]+cur_pt[1])/(2*(cur_pt[0]+cur_deltapt[0]/1000.)) > 1.4:
    #         mc_weight_jz0[eventNumber_jz0==ev_num] = 0.

        

# for ev_num in np.unique(eventNumber_jz1):
#     if eventNumber_jz1[eventNumber_jz1==ev_num].shape[0]>1:
#         this_event_pt = pt_jz1[eventNumber_jz1==ev_num].argsort()
#         cur_pt = pt_jz1[eventNumber_jz1==ev_num]
#         cur_pt = cur_pt[this_event_pt[::-1]]
#         cur_deltapt = deltapt_jz1[eventNumber_jz1==ev_num]
#         cur_deltapt = cur_deltapt[this_event_pt[::-1]]
        
#         if np.abs(cur_pt[0]+cur_pt[1])/(2*(cur_pt[0] - cur_deltapt[0]/1000.)) > 1.4:
#             mc_weight_jz1[eventNumber_jz1==ev_num] = 0.


############################################################################

# pt validation plots stacked

ymin = 1e-7
ymax = 1e4
logplot = True

bin_min = 0
bin_max = 300
bin_number = 300

pt0, bins = np.histogram(pt_jz0, bins=bin_number, range=(bin_min,bin_max), weights=mc_weight_jz0*weight_jz0)
pt1, bins = np.histogram(pt_jz1, bins=bin_number, range=(bin_min,bin_max), weights=mc_weight_jz1*weight_jz1)
pt2, bins = np.histogram(pt_jz2, bins=bin_number, range=(bin_min,bin_max), weights=mc_weight_jz2*weight_jz2)
pt3, bins = np.histogram(pt_jz3, bins=bin_number, range=(bin_min,bin_max), weights=mc_weight_jz3*weight_jz3)
pt4, bins = np.histogram(pt_jz4, bins=bin_number, range=(bin_min,bin_max), weights=mc_weight_jz4*weight_jz4)

plt.figure(figsize=(12,8))
bottom = np.zeros_like(bins[1:], dtype=float)
pt_label = ["$p_T$ of JZ4", "$p_T$ of JZ3","$p_T$ of JZ2","$p_T$ of JZ1","$p_T$ of JZ0"]
pt_color = ["violet","r","g","orange","blue"]
for i, pt_JZ in enumerate([pt4,pt3,pt2,pt1,pt0]):
    plt.bar((bins[1:]+bins[:-1])/2, pt_JZ, bottom=bottom, width =(bin_max-bin_min)/bin_number, label=pt_label[i], color=pt_color[i])
    bottom += pt_JZ
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
plt.xlabel("$p_T$ [GeV]", fontsize=20)
plt.ylabel("Number of Jets", fontsize=20)
if logplot:
    plt.ylim(ymin,ymax)
    plt.yscale('log')
plt.legend(fontsize=20, frameon=False)
plt.savefig(reference + "/" + "pt_dist.png", format='png', bbox_inches='tight')
plt.close()


############################################################################

# pt validation plots single

# plt.figure(figsize=(12,8))
# plt.step(bins[1:], pt0, c='blue', label="$p_T$ of JZ0")
# plt.step(bins[1:], pt1, c='orange', label="$p_T$ of JZ1")
# plt.step(bins[1:], pt2, c='green', label="$p_T$ of JZ2")
# plt.step(bins[1:], pt3, c='red', label="$p_T$ of JZ3")
# plt.step(bins[1:], pt4, c='violet', label="$p_T$ of JZ4")
# plt.xticks(fontsize=20)
# plt.yticks(fontsize=20)
# plt.xlabel("$p_T$ [GeV]", fontsize=20)
# plt.ylabel("Number of Jets", fontsize=20)
# if logplot:
#     plt.ylim(ymin,ymax)
#     plt.yscale('log')
# plt.legend(fontsize=20, frameon=False)
# plt.savefig(reference + "/" + "pt_not_stacked.png", format='png', bbox_inches='tight')
# plt.close()


############################################################################

# Leading pt plot

def get_leading_pt(cur_pt, cur_eN, cur_mc_weight, jz1=False):

    pt_array = np.array([])
    weight_array = np.array([])
    for ev_num in np.unique(cur_eN):
        this_event_pt = cur_pt[cur_eN==ev_num].argsort()
        this_event_mc_weight = cur_mc_weight[cur_eN==ev_num]
        pt = cur_pt[cur_eN==ev_num]
        pt = pt[this_event_pt[::-1]]
        pt_array = np.append(pt_array, pt[0])            
        if jz1 and pt[0]>90:
            weight_array = np.append(weight_array, 0.)
        else:
            weight_array = np.append(weight_array, this_event_mc_weight[0])
    return pt_array, weight_array



pt_jz0_leading, weight_leading_pt_jz0 = get_leading_pt(pt_jz0, eventNumber_jz0, mc_weight_jz0)
pt_jz1_leading, weight_leading_pt_jz1 = get_leading_pt(pt_jz1, eventNumber_jz1, mc_weight_jz1, jz1=True)
pt_jz2_leading, weight_leading_pt_jz2 = get_leading_pt(pt_jz2, eventNumber_jz2, mc_weight_jz2)
pt_jz3_leading, weight_leading_pt_jz3 = get_leading_pt(pt_jz3, eventNumber_jz3, mc_weight_jz3)
pt_jz4_leading, weight_leading_pt_jz4 = get_leading_pt(pt_jz4, eventNumber_jz4, mc_weight_jz4)

pt0, bins = np.histogram(pt_jz0_leading, bins=bin_number, range=(bin_min,bin_max), weights=weight_leading_pt_jz0*weight_jz0)
pt1, bins = np.histogram(pt_jz1_leading, bins=bin_number, range=(bin_min,bin_max), weights=weight_leading_pt_jz1*weight_jz1)
pt2, bins = np.histogram(pt_jz2_leading, bins=bin_number, range=(bin_min,bin_max), weights=weight_leading_pt_jz2*weight_jz2)
pt3, bins = np.histogram(pt_jz3_leading, bins=bin_number, range=(bin_min,bin_max), weights=weight_leading_pt_jz3*weight_jz3)
pt4, bins = np.histogram(pt_jz4_leading, bins=bin_number, range=(bin_min,bin_max), weights=weight_leading_pt_jz4*weight_jz4)


plt.figure(figsize=(12,8))
bottom = np.zeros_like(bins[1:], dtype=float)
pt_label = ["$p_T$ of JZ4", "$p_T$ of JZ3","$p_T$ of JZ2","$p_T$ of JZ1","$p_T$ of JZ0"]
pt_color = ["violet","r","g","orange","blue"]
for i, pt_JZ in enumerate([pt4,pt3,pt2,pt1,pt0]):
    plt.bar((bins[1:]+bins[:-1])/2, pt_JZ, bottom=bottom, width =(bin_max-bin_min)/bin_number, label=pt_label[i], color=pt_color[i])
    bottom += pt_JZ
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
plt.xlabel("Leading $p_T$ [GeV]", fontsize=20)
plt.ylabel("Number of Events", fontsize=20)
if logplot:
    plt.ylim(ymin,ymax)
    plt.yscale('log')
plt.legend(fontsize=20, frameon=False)
plt.savefig(reference + "/" + "leading_pt_dist.png", format='png', bbox_inches='tight')
plt.close()

# plt.figure(figsize=(12,8))
# # plt.step(bins[1:], pt0, c='blue', label="$p_T$ of JZ0")
# plt.step(bins[1:], pt1, c='orange', label="$p_T$ of JZ1")
# plt.step(bins[1:], pt2, c='green', label="$p_T$ of JZ2")
# plt.step(bins[1:], pt3, c='red', label="$p_T$ of JZ3")
# # plt.step(bins[1:], pt4, c='violet', label="$p_T$ of JZ4")
# plt.xticks(fontsize=20)
# plt.yticks(fontsize=20)
# plt.xlabel("Leading $p_T$ [GeV]", fontsize=20)
# plt.ylabel("Number of Events", fontsize=20)
# if logplot:
#     plt.ylim(ymin,ymax)
#     plt.yscale('log')
# plt.legend(fontsize=20, frameon=False)
# plt.savefig(reference + "/" + "leading_pt_not_stacked.png", format='png', bbox_inches='tight')
# plt.close()



############################################################################

# Truth pt plot

# ymin = 1e-9
# ymax = 1e3
# logplot = True

# bin_min = 0
# bin_max = 300
# bin_number = 300

# # pt0, bins = np.histogram(pt_jz0-deltapt_jz0/1000, bins=bin_number, range=(bin_min,bin_max), weights=mc_weight_jz0*weight_jz0)
# pt1, bins = np.histogram(pt_jz1-deltapt_jz1/1000, bins=bin_number, range=(bin_min,bin_max), weights=mc_weight_jz1*weight_jz1)
# pt2, bins = np.histogram(pt_jz2-deltapt_jz2/1000, bins=bin_number, range=(bin_min,bin_max), weights=mc_weight_jz2*weight_jz2)
# pt3, bins = np.histogram(pt_jz3-deltapt_jz3/1000, bins=bin_number, range=(bin_min,bin_max), weights=mc_weight_jz3*weight_jz3)
# # pt4, bins = np.histogram(pt_jz4-deltapt_jz4/1000, bins=bin_number, range=(bin_min,bin_max), weights=mc_weight_jz4*weight_jz4)

# plt.figure(figsize=(12,8))
# bottom = np.zeros_like(bins[1:], dtype=float)
# pt_label = ["$p_T$ of JZ4", "$p_T$ of JZ3","$p_T$ of JZ2","$p_T$ of JZ1"]#,"$p_T$ of JZ0"]
# pt_color = ["violet","r","g","orange"]#,"blue"]
# for i, pt_JZ in enumerate([pt4,pt3,pt2,pt1]):#,pt0]):
#     plt.bar((bins[1:]+bins[:-1])/2, pt_JZ, bottom=bottom, width =(bin_max-bin_min)/bin_number, label=pt_label[i], color=pt_color[i])
#     bottom += pt_JZ
# plt.xticks(fontsize=20)
# plt.yticks(fontsize=20)
# plt.xlabel("$p_T$ [GeV]", fontsize=20)
# plt.ylabel("Number of Jets", fontsize=20)
# plt.xlim(0,100)
# if logplot:
#     plt.ylim(ymin,ymax)
#     plt.yscale('log')
# plt.legend(fontsize=20, frameon=False)
# plt.savefig(reference + "/" + "pt_truth.png", format='png', bbox_inches='tight')
# plt.close()




############################################################################

# Truth pt ttbar plot

# ymin = 1e-7
# ymax = 1e5
# logplot = True

# bin_min = 0
# bin_max = 30
# bin_number = 300

# pt0, bins = np.histogram(pt_sig-deltapt_sig/1000, bins=bin_number, range=(bin_min,bin_max))

# plt.figure(figsize=(12,8))
# plt.step(bins[1:], pt0, c='blue', label="$p_T$ of ttbar")
# plt.xticks(fontsize=20)
# plt.yticks(fontsize=20)
# plt.xlabel("Truth $p_T$ [GeV]", fontsize=20)
# plt.ylabel("Number of Jets", fontsize=20)
# plt.xlim(0,30)

# if logplot:
#     plt.ylim(ymin,ymax)
#     plt.yscale('log')
# plt.legend(fontsize=20, frameon=False)
# plt.savefig(reference + "/" + "pt_truth_ttbar.png", format='png', bbox_inches='tight')
# plt.close()



############################################################################

# Visualize JZ1 weights

# bin_min = 0
# bin_max =  mc_weight_jz1.max()
# bin_number = 100

# _, weight_leading_pt_jz1 = get_leading_pt(pt_jz1, eventNumber_jz1, mc_weight_jz1, jz1=True)
# w1_1, bins = np.histogram(weight_leading_pt_jz1, bins=bin_number, range=(bin_min,bin_max))

# _, weight_leading_pt_jz1 = get_leading_pt(pt_jz1, eventNumber_jz1, mc_weight_jz1)
# w1_2, bins = np.histogram(weight_leading_pt_jz1, bins=bin_number, range=(bin_min,bin_max))


# plt.figure(figsize=(12,8))
# plt.step(bins[1:], w1_2, c='red', label="JZ1 weights")
# plt.step(bins[1:], w1_1, c='blue', label="JZ1 weights for $p_T$<90GeV")

# plt.xticks(fontsize=20)
# plt.yticks(fontsize=20)
# plt.xlabel("Weights", fontsize=20)
# plt.ylabel("Number of Events", fontsize=20)
# # plt.ylim(ymin,ymax)
# plt.yscale('log')
# plt.legend(fontsize=20, frameon=False)
# plt.savefig(reference + "/" + "weights.png", format='png', bbox_inches='tight')
# plt.close()



############################################################################

# pt signal plot

ymin = 1e-7
ymax = 1e5
logplot = True

bin_min = 0
bin_max = 300
bin_number = 300

pt0, bins = np.histogram(pt_sig, bins=bin_number, range=(bin_min,bin_max))

plt.figure(figsize=(12,8))
plt.step(bins[1:], pt0, c='blue', label="$p_T$ of ttbar")
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
plt.xlabel("$p_T$ [GeV]", fontsize=20)
plt.ylabel("Number of Jets", fontsize=20)

if logplot:
    plt.ylim(ymin,ymax)
    plt.yscale('log')
plt.legend(fontsize=20, frameon=False)
plt.savefig(reference + "/" + "pt_signal.png", format='png', bbox_inches='tight')
plt.close()



############################################################################













############################################################################
print("Plotting successful.")
