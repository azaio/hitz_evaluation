
#=============================================
# Script to evaluate salt test in a fast way
#=============================================

# Mo's data Dipz

# salt test --config logs/Dipz_20240126-T175203/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/mo_files/user.backes.601479.e8514_e8528_s4159_s4114_r14774_r14754.tdd.trigger_emtopo_data.24_0_16.r12684_r12782.v1-208-gc25c7c3_output.h5/vds/vds.h5 --ckpt_path logs/Dipz_20240126-T175203/ckpts/epoch\=113-val_loss\=-2.67856.ckpt
# mv logs/Dipz_20240126-T175203/ckpts/epoch=113-val_loss=-2.67856__test_vds.h5 logs/Dipz_20240126-T175203/ckpts/DiHiggs_test.h5

# salt test --config logs/Dipz_20240126-T175203/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/mo_files/user.backes.801165.e8514_e8528_s4159_s4114_r14774_r14754.tdd.trigger_emtopo_data.24_0_16.r12684_r12782.v1-208-gc25c7c3_output.h5/vds/vds.h5 --ckpt_path logs/Dipz_20240126-T175203/ckpts/epoch\=113-val_loss\=-2.67856.ckpt
# mv logs/Dipz_20240126-T175203/ckpts/epoch=113-val_loss=-2.67856__test_vds.h5 logs/Dipz_20240126-T175203/ckpts/JZ0_test.h5
# salt test --config logs/Dipz_20240126-T175203/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/mo_files/user.backes.801166.e8514_e8528_s4159_s4114_r14774_r14754.tdd.trigger_emtopo_data.24_0_16.r12684_r12782.v1-208-gc25c7c3_output.h5/vds/vds.h5 --ckpt_path logs/Dipz_20240126-T175203/ckpts/epoch\=113-val_loss\=-2.67856.ckpt
# mv logs/Dipz_20240126-T175203/ckpts/epoch=113-val_loss=-2.67856__test_vds.h5 logs/Dipz_20240126-T175203/ckpts/JZ1_test.h5
# salt test --config logs/Dipz_20240126-T175203/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/mo_files/user.backes.801167.e8514_e8528_s4159_s4114_r14774_r14754.tdd.trigger_emtopo_data.24_0_16.r12684_r12782.v1-208-gc25c7c3_output.h5/vds/vds.h5 --ckpt_path logs/Dipz_20240126-T175203/ckpts/epoch\=113-val_loss\=-2.67856.ckpt
# mv logs/Dipz_20240126-T175203/ckpts/epoch=113-val_loss=-2.67856__test_vds.h5 logs/Dipz_20240126-T175203/ckpts/JZ2_test.h5
# salt test --config logs/Dipz_20240126-T175203/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/mo_files/user.backes.801168.e8514_e8528_s4159_s4114_r14774_r14754.tdd.trigger_emtopo_data.24_0_16.r12684_r12782.v1-208-gc25c7c3_output.h5/vds/vds.h5 --ckpt_path logs/Dipz_20240126-T175203/ckpts/epoch\=113-val_loss\=-2.67856.ckpt
# mv logs/Dipz_20240126-T175203/ckpts/epoch=113-val_loss=-2.67856__test_vds.h5 logs/Dipz_20240126-T175203/ckpts/JZ3_test.h5
# salt test --config logs/Dipz_20240126-T175203/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/mo_files/user.backes.801169.e8514_e8528_s4159_s4114_r14774_r14754.tdd.trigger_emtopo_data.24_0_16.r12684_r12782.v1-208-gc25c7c3_output.h5/vds/vds.h5 --ckpt_path logs/Dipz_20240126-T175203/ckpts/epoch\=113-val_loss\=-2.67856.ckpt
# mv logs/Dipz_20240126-T175203/ckpts/epoch=113-val_loss=-2.67856__test_vds.h5 logs/Dipz_20240126-T175203/ckpts/JZ4_test.h5

#---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

# Mathias' data Dipz

# salt test --config logs/Dipz_20240126-T175203/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/Dipz_train/pp_output_test_ttbar.h5 --ckpt_path logs/Dipz_20240126-T175203/ckpts/epoch\=113-val_loss\=-2.67856.ckpt
# mv logs/Dipz_20240126-T175203/ckpts/epoch=113-val_loss=-2.67856__test_ttbar.h5 logs/Dipz_20240126-T175203/ckpts/mathias_ttbar_test.h5

# salt test --config logs/Dipz_20240126-T175203/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/200hits/user.backes.801165.e8514_e8528_s4111_s4114_r14634.tdd.trigger_hits.24_0_16.r12684_r12782.v1-200-g2a6de5b_output.h5/*.h5 --ckpt_path logs/Dipz_20240126-T175203/ckpts/epoch\=113-val_loss\=-2.67856.ckpt
# mv logs/Dipz_20240126-T175203/ckpts/epoch=113-val_loss=-2.67856__test_user.backes.36944946._000001.output.h5 logs/Dipz_20240126-T175203/ckpts/mathias_JZ0_test.h5
# salt test --config logs/Dipz_20240126-T175203/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/200hits/user.backes.801166.e8514_e8528_s4111_s4114_r14634.tdd.trigger_hits.24_0_16.r12684_r12782.v1-200-g2a6de5b_output.h5/*.h5 --ckpt_path logs/Dipz_20240126-T175203/ckpts/epoch\=113-val_loss\=-2.67856.ckpt
# mv logs/Dipz_20240126-T175203/ckpts/epoch=113-val_losss=-2.67856__test_user.backes.36944948._000001.output.h5 logs/Dipz_20240126-T175203/ckpts/mathias_JZ1_test.h5
# salt test --config logs/Dipz_20240126-T175203/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/200hits/user.backes.801167.e8514_e8528_s4111_s4114_r14634.tdd.trigger_hits.24_0_16.r12684_r12782.v1-200-g2a6de5b_output.h5/*.h5 --ckpt_path logs/Dipz_20240126-T175203/ckpts/epoch\=113-val_loss\=-2.67856.ckpt
# mv logs/Dipz_20240126-T175203/ckpts/epoch=113-val_loss=-2.67856__test_user.backes.36944950._000001.output.h5 logs/Dipz_20240126-T175203/ckpts/mathias_JZ2_test.h5
# salt test --config logs/Dipz_20240126-T175203/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/200hits/user.backes.801168.e8514_e8528_s4111_s4114_r14634.tdd.trigger_hits.24_0_16.r12684_r12782.v1-200-g2a6de5b_output.h5/*.h5 --ckpt_path logs/Dipz_20240126-T175203/ckpts/epoch\=113-val_loss\=-2.67856.ckpt
# mv logs/Dipz_20240126-T175203/ckpts/epoch=113-val_loss=-2.67856__test_user.backes.36944951._000001.output.h5 logs/Dipz_20240126-T175203/ckpts/mathias_JZ3_test.h5
# salt test --config logs/Dipz_20240126-T175203/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/200hits/user.backes.801169.e8514_e8528_s4111_s4114_r14634.tdd.trigger_hits.24_0_16.r12684_r12782.v1-200-g2a6de5b_output.h5/user.backes.36944947._000001*.h5 --ckpt_path logs/Dipz_20240126-T175203/ckpts/epoch\=113-val_loss\=-2.67856.ckpt
# mv logs/Dipz_20240126-T175203/ckpts/epoch=113-val_loss=-2.67856__test_user.backes.36944947._000001.output.h5 logs/Dipz_20240126-T175203/ckpts/mathias_JZ4_test.h5

# ls logs/Dipz_20240126-T175203/ckpts/

#---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

# Hitz 50

# salt test --config logs/Hitz_ttbar_50_20240201-T181358/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/50hits/pp_output_test_ttbar.h5 --ckpt_path logs/Hitz_ttbar_50_20240201-T181358/ckpts/epoch\=063-val_loss\=-0.47346.ckpt
# mv logs/Hitz_ttbar_50_20240201-T181358/ckpts/epoch\=063-val_loss\=-0.47346__test_ttbar.h5 logs/Hitz_ttbar_50_20240201-T181358/ckpts/mathias_ttbar_test.h5

# salt test --config logs/Hitz_ttbar_50_20240201-T181358/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/50hits/user.backes.801165.e8514_e8528_s4111_s4114_r14634.tdd.trigger_hits.24_0_21.r12684_r12782.v1-218-g627e0c6_output.h5/*.h5 --ckpt_path logs/Hitz_ttbar_50_20240201-T181358/ckpts/epoch\=063-val_loss\=-0.47346.ckpt
# mv logs/Hitz_ttbar_50_20240201-T181358/ckpts/epoch\=063-val_loss\=-0.47346__test_user.backes.37118157._000001.output.h5 logs/Hitz_ttbar_50_20240201-T181358/ckpts/mathias_JZ0_test.h5
# salt test --config logs/Hitz_ttbar_50_20240201-T181358/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/50hits/user.backes.801166.e8514_e8528_s4111_s4114_r14634.tdd.trigger_hits.24_0_21.r12684_r12782.v1-218-g627e0c6_output.h5/*.h5 --ckpt_path logs/Hitz_ttbar_50_20240201-T181358/ckpts/epoch\=063-val_loss\=-0.47346.ckpt
# mv logs/Hitz_ttbar_50_20240201-T181358/ckpts/epoch\=063-val_loss\=-0.47346__test_user.backes.37118162._000001.output.h5 logs/Hitz_ttbar_50_20240201-T181358/ckpts/mathias_JZ1_test.h5
# salt test --config logs/Hitz_ttbar_50_20240201-T181358/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/50hits/user.backes.801167.e8514_e8528_s4111_s4114_r14634.tdd.trigger_hits.24_0_21.r12684_r12782.v1-218-g627e0c6_output.h5/*.h5 --ckpt_path logs/Hitz_ttbar_50_20240201-T181358/ckpts/epoch\=063-val_loss\=-0.47346.ckpt
# mv logs/Hitz_ttbar_50_20240201-T181358/ckpts/epoch\=063-val_loss\=-0.47346__test_user.backes.37118158._000001.output.h5 logs/Hitz_ttbar_50_20240201-T181358/ckpts/mathias_JZ2_test.h5
# salt test --config logs/Hitz_ttbar_50_20240201-T181358/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/50hits/user.backes.801168.e8514_e8528_s4111_s4114_r14634.tdd.trigger_hits.24_0_21.r12684_r12782.v1-218-g627e0c6_output.h5/*.h5 --ckpt_path logs/Hitz_ttbar_50_20240201-T181358/ckpts/epoch\=063-val_loss\=-0.47346.ckpt
# mv logs/Hitz_ttbar_50_20240201-T181358/ckpts/epoch\=063-val_loss\=-0.47346__test_user.backes.37118161._000001.output.h5 logs/Hitz_ttbar_50_20240201-T181358/ckpts/mathias_JZ3_test.h5
# salt test --config logs/Hitz_ttbar_50_20240201-T181358/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/50hits/user.backes.801169.e8514_e8528_s4111_s4114_r14634.tdd.trigger_hits.24_0_21.r12684_r12782.v1-218-g627e0c6_output.h5/user.backes.37118160._000001*.h5 --ckpt_path logs/Hitz_ttbar_50_20240201-T181358/ckpts/epoch\=063-val_loss\=-0.47346.ckpt
# mv logs/Hitz_ttbar_50_20240201-T181358/ckpts/epoch\=063-val_loss\=-0.47346__test_user.backes.37118160._000001.output.h5 logs/Hitz_ttbar_50_20240201-T181358/ckpts/mathias_JZ4_test.h5

# ls logs/Hitz_ttbar_50_20240201-T181358/ckpts

#---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

# Hitz 200

# salt test --config logs/Hitz_ttbar_20240129-T115654/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/Hitz_train/pp_output_test_ttbar.h5 --ckpt_path logs/Hitz_ttbar_20240129-T115654/ckpts/epoch\=077-val_loss\=-1.74109.ckpt
# mv logs/Hitz_ttbar_20240129-T115654/ckpts/epoch=077-val_loss=-1.74109__test_ttbar.h5 logs/Hitz_ttbar_20240129-T115654/ckpts/mathias_ttbar_test.h5

# salt test --config logs/Hitz_ttbar_20240129-T115654/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/200hits/user.backes.801165.e8514_e8528_s4111_s4114_r14634.tdd.trigger_hits.24_0_16.r12684_r12782.v1-200-g2a6de5b_output.h5/*.h5 --ckpt_path logs/Hitz_ttbar_20240129-T115654/ckpts/epoch\=077-val_loss\=-1.74109.ckpt
# mv logs/Hitz_ttbar_20240129-T115654/ckpts/epoch=077-val_loss=-1.74109__test_user.backes.36944946._000001.output.h5 logs/Hitz_ttbar_20240129-T115654/ckpts/mathias_JZ0_test.h5
# salt test --config logs/Hitz_ttbar_20240129-T115654/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/200hits/user.backes.801166.e8514_e8528_s4111_s4114_r14634.tdd.trigger_hits.24_0_16.r12684_r12782.v1-200-g2a6de5b_output.h5/*.h5 --ckpt_path logs/Hitz_ttbar_20240129-T115654/ckpts/epoch\=077-val_loss\=-1.74109.ckpt
# mv logs/Hitz_ttbar_20240129-T115654/ckpts/epoch=077-val_loss=-1.74109__test_user.backes.36944948._000001.output.h5 logs/Hitz_ttbar_20240129-T115654/ckpts/mathias_JZ1_test.h5
# salt test --config logs/Hitz_ttbar_20240129-T115654/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/200hits/user.backes.801167.e8514_e8528_s4111_s4114_r14634.tdd.trigger_hits.24_0_16.r12684_r12782.v1-200-g2a6de5b_output.h5/*.h5 --ckpt_path logs/Hitz_ttbar_20240129-T115654/ckpts/epoch\=077-val_loss\=-1.74109.ckpt
# mv logs/Hitz_ttbar_20240129-T115654/ckpts/epoch=077-val_loss=-1.74109__test_user.backes.36944950._000001.output.h5 logs/Hitz_ttbar_20240129-T115654/ckpts/mathias_JZ2_test.h5
# salt test --config logs/Hitz_ttbar_20240129-T115654/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/200hits/user.backes.801168.e8514_e8528_s4111_s4114_r14634.tdd.trigger_hits.24_0_16.r12684_r12782.v1-200-g2a6de5b_output.h5/*.h5 --ckpt_path logs/Hitz_ttbar_20240129-T115654/ckpts/epoch\=077-val_loss\=-1.74109.ckpt
# mv logs/Hitz_ttbar_20240129-T115654/ckpts/epoch=077-val_loss=-1.74109__test_user.backes.36944951._000001.output.h5 logs/Hitz_ttbar_20240129-T115654/ckpts/mathias_JZ3_test.h5
# salt test --config logs/Hitz_ttbar_20240129-T115654/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/200hits/user.backes.801169.e8514_e8528_s4111_s4114_r14634.tdd.trigger_hits.24_0_16.r12684_r12782.v1-200-g2a6de5b_output.h5/user.backes.36944947._000001*.h5 --ckpt_path logs/Hitz_ttbar_20240129-T115654/ckpts/epoch\=077-val_loss\=-1.74109.ckpt
# mv logs/Hitz_ttbar_20240129-T115654/ckpts/epoch=077-val_loss=-1.74109__test_user.backes.36944947._000001.output.h5 logs/Hitz_ttbar_20240129-T115654/ckpts/mathias_JZ4_test.h5

# ls logs/Hitz_ttbar_20240129-T115654/ckpts

# #---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

# # Hitz DiHiggs

# salt test --config logs/Hitz_new_pp_large_lr_200_20240312-T090531/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/DiHiggs_untriggered/user.backes.*.h5/*.h5 --ckpt_path logs/Hitz_new_pp_large_lr_200_20240312-T090531/ckpts/epoch=140-val_loss=-1.54826.ckpt

# salt test --config logs/Hitz_new_pp_large_lr_200_20240312-T090531config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/10pttruth/user.backes.801165.*.h5/*.h5 --ckpt_path logs/Hitz_new_pp_large_lr_200_20240312-T090531/ckpts/epoch=140-val_loss=-1.54826.ckpt
# salt test --config logs/Hitz_new_pp_large_lr_200_20240312-T090531/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/10pttruth/user.backes.801166.*.h5/*.h5 --ckpt_path logs/Hitz_new_pp_large_lr_200_20240312-T090531/ckpts/epoch=140-val_loss=-1.54826.ckpt
# salt test --config logs/Hitz_new_pp_large_lr_200_20240312-T090531/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/10pttruth/user.backes.801167.*.h5/*.h5 --ckpt_path logs/Hitz_new_pp_large_lr_200_20240312-T090531/ckpts/epoch=140-val_loss=-1.54826.ckpt
# salt test --config logs/Hitz_new_pp_large_lr_200_20240312-T090531/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/10pttruth/user.backes.801168.*.h5/*.h5 --ckpt_path logs/Hitz_new_pp_large_lr_200_20240312-T090531/ckpts/epoch=140-val_loss=-1.54826.ckpt

#---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

# # Hitz  L1_J45p0ETA21_3J15p0ETA25

# salt test --config logs/Hitz_new_pp_large_lr_200_20240312-T090531/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/L1_trigger/user.backes.601479*.h5/*.h5 --ckpt_path logs/Hitz_new_pp_large_lr_200_20240312-T090531/ckpts/epoch=140-val_loss=-1.54826.ckpt
# salt test --config logs/Hitz_new_pp_large_lr_200_20240312-T090531/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/L1_trigger/user.backes.601229*.h5/*.h5 --ckpt_path logs/Hitz_new_pp_large_lr_200_20240312-T090531/ckpts/epoch=140-val_loss=-1.54826.ckpt

# salt test --config logs/Hitz_new_pp_large_lr_200_20240312-T090531/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/L1_trigger/user.backes.801165*.h5/*.h5 --ckpt_path logs/Hitz_new_pp_large_lr_200_20240312-T090531/ckpts/epoch=140-val_loss=-1.54826.ckpt
# salt test --config logs/Hitz_new_pp_large_lr_200_20240312-T090531/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/L1_trigger/user.backes.801166*.h5/*.h5 --ckpt_path logs/Hitz_new_pp_large_lr_200_20240312-T090531/ckpts/epoch=140-val_loss=-1.54826.ckpt
# salt test --config logs/Hitz_new_pp_large_lr_200_20240312-T090531/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/L1_trigger/user.backes.801167*.h5/*.h5 --ckpt_path logs/Hitz_new_pp_large_lr_200_20240312-T090531/ckpts/epoch=140-val_loss=-1.54826.ckpt
# salt test --config logs/Hitz_new_pp_large_lr_200_20240312-T090531/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/L1_trigger/user.backes.801168*.h5/*.h5 --ckpt_path logs/Hitz_new_pp_large_lr_200_20240312-T090531/ckpts/epoch=140-val_loss=-1.54826.ckpt

#---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

# Hitz L1_jJ85p0ETA21_3jJ40p0ETA25

# salt test --config logs/Hitz_new_pp_large_lr_200_20240312-T090531/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/L1_jfex/user.backes.601479*.h5/*.h5 --ckpt_path logs/Hitz_new_pp_large_lr_200_20240312-T090531/ckpts/epoch=140-val_loss=-1.54826.ckpt
# salt test --config logs/Hitz_new_pp_large_lr_200_20240312-T090531/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/L1_jfex/user.backes.601229*.h5/*.h5 --ckpt_path logs/Hitz_new_pp_large_lr_200_20240312-T090531/ckpts/epoch=140-val_loss=-1.54826.ckpt

# salt test --config logs/Hitz_new_pp_large_lr_200_20240312-T090531/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/L1_jfex/user.backes.801165*.h5/*.h5 --ckpt_path logs/Hitz_new_pp_large_lr_200_20240312-T090531/ckpts/epoch=140-val_loss=-1.54826.ckpt
# salt test --config logs/Hitz_new_pp_large_lr_200_20240312-T090531/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/L1_jfex/user.backes.801166*.h5/*.h5 --ckpt_path logs/Hitz_new_pp_large_lr_200_20240312-T090531/ckpts/epoch=140-val_loss=-1.54826.ckpt
# salt test --config logs/Hitz_new_pp_large_lr_200_20240312-T090531/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/L1_jfex/user.backes.801167*.h5/*.h5 --ckpt_path logs/Hitz_new_pp_large_lr_200_20240312-T090531/ckpts/epoch=140-val_loss=-1.54826.ckpt
# salt test --config logs/Hitz_new_pp_large_lr_200_20240312-T090531/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/L1_jfex/user.backes.801168*.h5/*.h5 --ckpt_path logs/Hitz_new_pp_large_lr_200_20240312-T090531/ckpts/epoch=140-val_loss=-1.54826.ckpt

#---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

# Dipz L1_jJ85p0ETA21_3jJ40p0ETA25

# salt test --config logs/Dipz_BIG_ttbar_20240315-T180304/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/L1_jfex/user.backes.601479*.h5/*.h5 --ckpt_path logs/Dipz_BIG_ttbar_20240315-T180304/ckpts/epoch\=133-val_loss\=-2.78947.ckpt
# salt test --config logs/Dipz_BIG_ttbar_20240315-T180304/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/L1_jfex/user.backes.601229*.h5/*.h5 --ckpt_path logs/Dipz_BIG_ttbar_20240315-T180304/ckpts/epoch\=133-val_loss\=-2.78947.ckpt

# salt test --config logs/Dipz_BIG_ttbar_20240315-T180304/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/L1_jfex/user.backes.801165*.h5/*.h5 --ckpt_path logs/Dipz_BIG_ttbar_20240315-T180304/ckpts/epoch\=133-val_loss\=-2.78947.ckpt
# salt test --config logs/Dipz_BIG_ttbar_20240315-T180304/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/L1_jfex/user.backes.801166*.h5/*.h5 --ckpt_path logs/Dipz_BIG_ttbar_20240315-T180304/ckpts/epoch\=133-val_loss\=-2.78947.ckpt
# salt test --config logs/Dipz_BIG_ttbar_20240315-T180304/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/L1_jfex/user.backes.801167*.h5/*.h5 --ckpt_path logs/Dipz_BIG_ttbar_20240315-T180304/ckpts/epoch\=133-val_loss\=-2.78947.ckpt
# salt test --config logs/Dipz_BIG_ttbar_20240315-T180304/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/L1_jfex/user.backes.801168*.h5/*.h5 --ckpt_path logs/Dipz_BIG_ttbar_20240315-T180304/ckpts/epoch\=133-val_loss\=-2.78947.ckpt

#---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

# BTagging with jet constituents

salt test --config logs/BTag_hit_jC_20240429-T145111/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/jet_const/pp_output_test_ttbar.h5 --ckpt_path logs/BTag_hit_jC_20240429-T145111/ckpts/epoch\=030-val_loss\=7.08021.ckpt 
salt test --config logs/BTag_hit_20240429-T153631/config.yaml  --data.test_file /eos/user/b/backes/QT/preprocessing/jet_const/pp_output_test_ttbar.h5 --ckpt_path logs/BTag_hit_20240429-T153631/ckpts/epoch\=064-val_loss\=7.20974.ckpt 
salt test --config logs/BTag_jC_without_z_20240429-T144545/config.yaml --data.test_file /eos/user/b/backes/QT/preprocessing/jet_const/pp_output_test_ttbar.h5 --ckpt_path logs/BTag_jC_without_z_20240429-T144545/ckpts/epoch\=015-val_loss\=0.71456.ckpt 
