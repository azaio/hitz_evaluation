#######################################################################################################################################################

# Preprocessing for HitZ

#######################################################################################################################################################

# Imports

import h5py
import yaml
import glob
import numpy as np
# np.seterr(all="ignore")

#######################################################################################################################################################

# Overview

print("========================================================================================== \n" +
      "Preprocessing for HitZ \n==========================================================================================")

# Define datasets
dataset_numbers = ["601229"]#, "801165", "801166", "801167", "801168"]
dataset_names = ["ttbar"]#, "JZ0", "JZ1", "JZ2", "JZ3"]

# Specify input and output direction
in_dir = "/eos/user/b/backes/QT/preprocessing/10pttruth/"
out_dir = "/eos/user/b/backes/QT/preprocessing/test/"

# Set number of jets (recommended: -1 to evaluate all)
n_jets = 10000 #-1

# Specify variables
jet_variables = ["TruthJetPVz","eta","pt","eventNumber","mcEventWeight"]
hit_variables = ["j","a","b","valid"] # Salt needs "valid" for some reason

# Print some general features
print("Datasets: ", dataset_names)
print("Jet variables: ", jet_variables)
print("Hit variables: ", hit_variables)
print("------------------------------------------------------------------------------------------")


#######################################################################################################################################################

# Split and cut function

def split_datasets(dataset):
    """
    Split datasets and apply non-NaN selection as well as pt-cuts.
    """

    f = h5py.File(dataset, "r")
    ds_jets = f["jets"][:n_jets] 
    ds_hits = f["hits"][:n_jets]

    # pt cuts
    # ds_hits = ds_hits[ds_jets["pt"]<300000]
    # ds_jets = ds_jets[ds_jets["pt"]<300000]
    ds_hits = ds_hits[ds_jets["pt"]>20000]
    ds_jets = ds_jets[ds_jets["pt"]>20000]

    # Make sure that training variables have no NaN values
    ds_hits = ds_hits[~np.isnan(ds_jets["TruthJetPVz"])]
    ds_jets = ds_jets[~np.isnan(ds_jets["TruthJetPVz"])]
    ds_jets = ds_jets[~np.isnan(np.sum(ds_hits["j"], axis=1))]
    ds_hits = ds_hits[~np.isnan(np.sum(ds_hits["j"], axis=1))]

    # Split in train, test and validation
    ds_hits_train = ds_hits[ds_jets["eventNumber"]%10 <= 7]
    ds_jets_train = ds_jets[ds_jets["eventNumber"]%10 <= 7]
    ds_hits_test = ds_hits[ds_jets["eventNumber"]%10 == 8]
    ds_jets_test = ds_jets[ds_jets["eventNumber"]%10 == 8]
    ds_hits_val = ds_hits[ds_jets["eventNumber"]%10 == 9]
    ds_jets_val = ds_jets[ds_jets["eventNumber"]%10 == 9]

    return ds_jets_train, ds_hits_train, ds_jets_test, ds_hits_test, ds_jets_val, ds_hits_val

   
#######################################################################################################################################################

# Run cuts and splits for all input datasets

datasets = []
for d in dataset_numbers:
    datasets.append(glob.glob(in_dir + "user.*." + d + ".*/*.h5")[0])

ds_jets_train, ds_hits_train, ds_jets_test, ds_hits_test, ds_jets_val, ds_hits_val = split_datasets(datasets[0])

# ds_jets_train_full = ds_jets_train[jet_variables]
# ds_hits_train_full = ds_hits_train[hit_variables]
# ds_jets_test_full = ds_jets_test[jet_variables]
# ds_hits_test_full = ds_hits_test[hit_variables]
# ds_jets_val_full = ds_jets_val[jet_variables]
# ds_hits_val_full = ds_hits_val[hit_variables]

ds_hits_train_full = np.rec.fromarrays([ds_hits_train[var] for var in hit_variables], names=hit_variables)
ds_jets_test_full = np.rec.fromarrays([ds_jets_test[var] for var in jet_variables], names=jet_variables)
ds_hits_test_full = np.rec.fromarrays([ds_hits_test[var] for var in hit_variables], names=hit_variables)
ds_jets_val_full = np.rec.fromarrays([ds_jets_val[var] for var in jet_variables], names=jet_variables)
ds_hits_val_full = np.rec.fromarrays([ds_hits_val[var] for var in hit_variables], names=hit_variables)

test_splitter = np.array([0,ds_jets_test.shape[0]])
print("Cut & split done for ", dataset_names[0])

for i, ds in enumerate(datasets[1:]):

    ds_jets_train, ds_hits_train, ds_jets_test, ds_hits_test, ds_jets_val, ds_hits_val = split_datasets(ds)
    ds_jets_train_full = np.append(ds_jets_train_full, ds_jets_train[jet_variables])
    ds_hits_train_full = np.append(ds_hits_train_full, ds_hits_train[hit_variables], axis=0)        
    ds_jets_test_full = np.append(ds_jets_test_full, ds_jets_test[jet_variables])
    ds_hits_test_full = np.append(ds_hits_test_full, ds_hits_test[hit_variables], axis=0)
    ds_jets_val_full = np.append(ds_jets_val_full, ds_jets_val[jet_variables])
    ds_hits_val_full = np.append(ds_hits_val_full, ds_hits_val[hit_variables], axis=0)
    test_splitter = np.append(test_splitter, test_splitter[-1]+ds_jets_test.shape[0])
    print("Cut & split done for ", dataset_names[i+1])

print("------------------------------------------------------------------------------------------")


#######################################################################################################################################################

# Save data in h5 files 

train = h5py.File(out_dir + "pp_output_train.h5", "w")
train.create_dataset('jets', data=ds_jets_train_full)
train.create_dataset('hits', data=ds_hits_train_full)

val = h5py.File(out_dir + "pp_output_val.h5", "w")
val.create_dataset('jets', data=ds_jets_val_full)
val.create_dataset('hits', data=ds_hits_val_full)

for i, name in enumerate(dataset_names):
    test = h5py.File(out_dir + "pp_output_test_" + name + ".h5", "w")
    test.create_dataset('jets', data=ds_jets_test_full[test_splitter[i]:test_splitter[i+1]])
    test.create_dataset('hits', data=ds_hits_test_full[test_splitter[i]:test_splitter[i+1]])
    test.close()


#######################################################################################################################################################
    
# Calculate norm dictionary and save as yaml file

# Jets normalisation dictionary
jet_dict = dict()
for jet_var in jet_variables:
    cur_dict = dict()
    mean = np.mean(np.concatenate((ds_jets_train_full[jet_var],ds_jets_test_full[jet_var],ds_jets_val_full[jet_var])), dtype=float)
    std = np.std(np.concatenate((ds_jets_train_full[jet_var],ds_jets_test_full[jet_var],ds_jets_val_full[jet_var])), dtype=float)
    cur_dict["mean"] = float(mean)
    cur_dict["std"] = float(std)
    jet_dict[jet_var] = cur_dict

# Hits normalisation dictionary
hits_dict = dict()
for hit_var in hit_variables:
    cur_dict = dict()
    mean = np.mean(np.concatenate((ds_hits_train_full[hit_var],ds_hits_test_full[hit_var],ds_hits_val_full[hit_var])).flatten(), dtype=float)
    std = np.std(np.concatenate((ds_hits_train_full[hit_var],ds_hits_test_full[hit_var],ds_hits_val_full[hit_var])).flatten(), dtype=float)
    cur_dict["mean"] = float(mean)
    cur_dict["std"] = float(std)
    hits_dict[hit_var] = cur_dict

# Write normalisation dictionary yaml file
data = dict(jets = jet_dict, hits = hits_dict)
with open(out_dir + 'norm_dict.yaml', 'w') as outfile:
    yaml.dump(data, outfile, default_flow_style=False)

# Write class dictionary yaml file
classes = dict(jets = dict(), hits = dict())
with open(out_dir + 'class_dict.yaml', 'w') as outfile:
    yaml.dump(classes, outfile, default_flow_style=False)


print("Saving successfull.")
print("------------------------------------------------------------------------------------------")

#######################################################################################################################################################

print("Preprocessing done.")
