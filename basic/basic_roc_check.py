########################################################################################################################################################

# Main evaluation framework for HitZ

########################################################################################################################################################

# Required packages

import h5py
import numpy as np
import matplotlib.pyplot as plt
import itertools
import glob
import mplhep as hep
hep.style.use(hep.style.ATLAS)

print("\n=========================== \n" + " Calculation of ROC curves \n" + "=========================== \n")

########################################################################################################################################################

# Loading data

base_path = '/eos/user/b/backes/QT/preprocessing/'
network_base = '/eos/user/b/backes/QT/salt/salt/logs/' + "Hitz_new_pp_large_lr_200_20240312-T090531" + "/ckpts/epoch=*." 

test_path_1 = glob.glob(base_path + "DiHiggs_untriggered/user" + ".*.h5/*.h5")[0]
test_path_2 = glob.glob(base_path + "10pttruth/user.*.801166" + ".*.h5/*.h5")[0]
network_path_1 = glob.glob(network_base + "37738327" + ".*.h5")[0]
network_path_2 = glob.glob(network_base + "37402984" + ".*.h5")[0]


num_jets = -1 #10000

with h5py.File(test_path_1, 'r') as test_f:
    jets_sig = test_f['jets'][:num_jets]
    pt_MeV_sig = jets_sig['pt']
    pt_sig = pt_MeV_sig/1000
    eventNumber_sig = jets_sig['eventNumber']
    mc_weight_sig = jets_sig['mcEventWeight']
    # TruthZ_sig = jets_sig['TruthJetPVz']
    HadronConeExclTruthLabelID_sig = jets_sig['HadronConeExclTruthLabelID']
with h5py.File(network_path_1, 'r') as f:
    jets = f['jets'][:num_jets]
    jet_z_pred_sig = jets["gaussian_regression_TruthJetPVz"]
    jet_z_stddev_sig = jets["gaussian_regression_TruthJetPVz_stddev"]

with h5py.File(test_path_2, 'r') as test_f:
    jets_jz1 = test_f['jets'][:num_jets]
    pt_MeV_jz1 = jets_jz1['pt']
    pt_jz1 = pt_MeV_jz1/1000
    eventNumber_jz1 = jets_jz1['eventNumber']
    mc_weight_jz1 = jets_jz1['mcEventWeight']
with h5py.File(network_path_2, 'r') as f:
    jets = f['jets'][:num_jets]
    jet_z_pred_jz1 = jets["gaussian_regression_TruthJetPVz"]
    jet_z_stddev_jz1 = jets["gaussian_regression_TruthJetPVz_stddev"]


########################################################################################################################################################

# Include 4 jet condition for signal

# count_throw_away = 0
# sum_counter = 0 
# for id in np.unique(eventNumber_sig):
#     _, counts = np.unique(TruthZ_sig[eventNumber_sig == id], return_counts=True)
#     if len(eventNumber_sig[eventNumber_sig == id]) < 4:
#         sum_counter += 1
#     if max(counts) < 4 :
#         mc_weight_sig[eventNumber_sig == id] = 0.
#         count_throw_away += 1

# print("4-jet-condition done. Threw away {:0.2f}% of the test data.".format(100*sum_counter/len(np.unique(eventNumber_sig))))
# print("4-jet-condition from one vertex done. Threw away {:0.2f}% of the test data.".format(100*count_throw_away/len(np.unique(eventNumber_sig))))


########################################################################################################################################################

# Include 4 bjet condition for signal

count = 0
sum_counter = 0
for id in np.unique(eventNumber_sig):
    jets = HadronConeExclTruthLabelID_sig[eventNumber_sig == id]
    bjets = jets[jets == 5]
    if len(eventNumber_sig[eventNumber_sig == id]) < 4:
        sum_counter += 1
    if len(bjets) < 4 :
        mc_weight_sig[eventNumber_sig == id] = 0.
        count += 1

print("4-jet-condition done. Threw away {:0.2f}% of the test data".format(100*sum_counter/len(np.unique(eventNumber_sig))))
print("4-b-jet-condition done. Threw away {:0.2f}% of the test data.".format(100*count/len(np.unique(eventNumber_sig))))


########################################################################################################################################################

# Implementation of MLPL calculation

def calculate_L_max(ev_num, n, m, cur_eN, cur_pt, cur_pred, cur_std):
    """
    Function to calculate the MLPL(n,m) value of one event.
        Requires:
        - ev_num: the event number of the event at hand,
        - cur_eN: the event numbers of the full sample,
        - cur_pt: the pt values of the full sample,
        - cur_pred: the current z vertex prediction of the full sample,
        - cur_std: the current sigma uncertainty prediction of the full sample.
        Returns:
        - MLPL(n,m) value of the event.
    """
    # Get m leading pt jets 
    this_event_pt = cur_pt[cur_eN==ev_num].argsort()
    pred = cur_pred[cur_eN==ev_num]
    std = cur_std[cur_eN==ev_num]
    pred = pred[this_event_pt[::-1]]
    pred = pred[:m]
    std = std[this_event_pt[::-1]]
    std = std[:m]

    # Calculate MLPL(n,m) for each n-jet combinations out of the m jets
    L_max = -9999999999.
    all_jets = range(m)
    for subset in itertools.combinations(all_jets, n):
        log_l = -0.5*np.log(2*np.pi)*n
        log_l += -np.sum(np.log(std[[subset]]), dtype=float)
        term1 = np.sum(np.divide(pred[[subset]], std[[subset]]**2, dtype=float), dtype=float)
        term2 = np.sum(np.divide(np.ones_like(std[[subset]],dtype=float), std[[subset]]**2, dtype=float), dtype=float)
        log_l += -np.sum( np.divide((term1/term2 - pred[[subset]])**2,(2.*std[[subset]]**2), dtype=float))
        if log_l > L_max:
            L_max = log_l

    return L_max


def multiple_jet_roc_efficiencies_new(x, n, m, cur_eN, cur_pt, cur_pred, cur_std, cur_mc_weight):
    """
    Function to execute the MLPL(n,m) value of the full event sample.
        Requires:
        - x: the cut values to calculate efficiencies and ,
        - cur_eN: the event numbers of the full sample,
        - cur_pt: the pt values of the full sample,
        - cur_pred: the z vertex prediction of the full sample,
        - cur_std: the sigma uncertainty prediction of the full sample,
        - cur_mc_weight: the MC event weights of the full sample.
        Returns:
        - passed_events: events with at least n jets,
        - correct_events: passed_events, that also fulfill MLPL(n,m)>x,
        - cur_MPLP: MLPL(n,m) values of all the events,
        - cur_MLPL_weights: MC event weights of MLPL(n,m).
    """
    passed_events = 0.
    correct_events = np.zeros_like(x, dtype=float)
    cur_MLPL = np.array([])
    cur_MLPL_weights = np.array([])
    for ev_num in np.unique(cur_eN):
        jets = cur_eN[cur_eN==ev_num].shape[0]
        if jets >= n:
            passed_events += cur_mc_weight[cur_eN==ev_num][0]
            if m == "All":
                l_max = calculate_L_max(ev_num, n, jets, cur_eN, cur_pt, cur_pred, cur_std)
            else:
                if jets >= m: 
                    l_max = calculate_L_max(ev_num, n, m, cur_eN, cur_pt, cur_pred, cur_std)
                else:
                    l_max = calculate_L_max(ev_num, n, jets, cur_eN, cur_pt, cur_pred, cur_std)
            correct_events[x<l_max] += cur_mc_weight[cur_eN==ev_num][0]
            cur_MLPL = np.append(cur_MLPL, l_max)
            cur_MLPL_weights = np.append(cur_MLPL_weights, cur_mc_weight[cur_eN==ev_num][0])
    return passed_events, correct_events, cur_MLPL, cur_MLPL_weights


########################################################################################################################################################

# Execution for several values of n and m

n_list = [4]
m_list = [4,5,6,"All"]

x = np.linspace(-50, 40, 400, dtype=float)
sig =[]
bkg =[]
tuple_entry = []

for n in n_list:
    print("n = ", n)
    for m in m_list:
        if m != "All":
            if m < n:
                continue
        print("m = ", m)
        
        # Calculate signal efficiencies and background rejection
        passed_events_sig, correct_events_sig, MLPL_sig, MLPL_sig_weights = multiple_jet_roc_efficiencies_new(
            x, n, m, eventNumber_sig, pt_sig, jet_z_pred_sig, jet_z_stddev_sig, mc_weight_sig)
        passed_events_bkg, correct_events_bkg, MLPL_bkg, MLPL_bkg_weights = multiple_jet_roc_efficiencies_new(
            x, n, m, eventNumber_jz1, pt_jz1, jet_z_pred_jz1, jet_z_stddev_jz1, mc_weight_jz1)

        eff_sig = correct_events_sig / passed_events_sig
        correct_events_bkg[correct_events_bkg==0] = np.nan
        eff_bkg = correct_events_bkg / passed_events_bkg
        rej_bkg = 1./eff_bkg

        sig.append(eff_sig)
        bkg.append(rej_bkg)
        tuple_entry.append((n,m))


########################################################################################################################################################
    
    # Plotting of MLPL(n, All) values to see impact of the MC reweighting

        if m == "All":
            mlplS, bins = np.histogram(MLPL_sig, bins=100, range=(-25,5), weights=MLPL_sig_weights, density=True)
            mlplS_uw, bins = np.histogram(MLPL_sig, bins=100, range=(-25,5), density=True)
            mlplB, bins = np.histogram(MLPL_bkg, bins=100, range=(-25,5), weights=MLPL_bkg_weights, density=True)
            mlplB_uw, bins = np.histogram(MLPL_bkg, bins=100, range=(-25,5), density=True)            

            plt.figure(figsize=(12,8))
            plt.step(bins[1:], mlplS, c='red', label="MLPL(4,All) Signal")
            plt.step(bins[1:], mlplS_uw, c='orange', label="MLPL(4,All) Signal w/o reweighting")
            plt.step(bins[1:], mlplB, c='blue', label="MLPL(4,All) Bkg")
            plt.step(bins[1:], mlplB_uw, c='green', label="MLPL(4,All) Bkg w/o reweighting")
            plt.xticks(fontsize=20)
            plt.yticks(fontsize=20)
            plt.xlim(-24.9,4.9)
            plt.ylim(0,1)
            plt.xlabel("MLPL(4, All)", fontsize=20)
            plt.ylabel("Normalized Number of Events", fontsize=20)
            plt.legend(fontsize=20, frameon=False)
            hep.atlas.label(loc=4, data=True, label="Work in Progress", rlabel='')
            plt.savefig("basic_HitZ/MLPL(4,All).png", format='png', bbox_inches='tight')
            plt.close()


########################################################################################################################################################

# Plotting the ROC curves
            
x_ref = np.linspace(0.01, 1, 10000)
color = iter(plt.cm.rainbow(np.linspace(0, 1, len(tuple_entry))))

plt.figure(figsize=(12,8))
plt.rc('axes', axisbelow=True)
plt.grid(True, which="major", axis="x")
plt.grid(True, which="both", axis="y")
for l, entry in enumerate(tuple_entry):
    c = next(color)
    plt.plot(sig[l], bkg[l], marker='.', color=c, label="MLPL{}".format(entry))
plt.plot(x_ref, 1/x_ref, linestyle='--', color='black', label=r"$\epsilon_{\mathrm{sig}}= \epsilon_{\mathrm{bkg}}$")
plt.legend(fontsize=20, frameon=False)
plt.xlabel(r"Signal Efficiency $\epsilon_{\mathrm{sig}}$", fontsize=20)
plt.ylabel(r"Background Rejection $1/\epsilon_{\mathrm{bkg}}$", fontsize=20)
plt.xlim(0.9, 1)
plt.ylim(0.9,7e1)
plt.yscale('log')
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
hep.atlas.label(loc=4, data=True, label="Work in Progress", rlabel='')
plt.savefig("basic_HitZ/Roc_curves.png" , format='png', bbox_inches='tight')
plt.close()


########################################################################################################################################################

print("Evaluation successful.")