# HitZ_evaluation

Author: Mathias Backes

In case of questions or comments just contact me via: mathias.backes@kip.uni-heidelberg.de


## Overview

This is a repository to evaluate the performance of transformer networks trained with the [Salt framework](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/salt). The training and testing data is extracted from AOD files with the [training dataset dumper](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper) and preprocessed with [umami](https://github.com/umami-hep/umami-preprocessing).

There are three main applications based on hit-level information implemented:

- Pile-up rejection (following [DipZ](https://gitlab.cern.ch/maboelel/the_DIPZ_project))
- Improving ROI tracking
- b-tagging


## Directories

- basic: Use in order to evaluate a jet-by-jet HitZ training. If you have not worked with HitZ before, start here.
- btagging: To evaluate a b-tagging based on hit-level information.
- documentation: LaTeX repository for the ATLAS internal document.
- Full_Event: Make full-event PVz predictions by combining jet-by-jet predictions in an MLPL way. Potentially useful for ROI tracking.
- Hitz_Dipz_comparison: Kind of old repository to compare the HitZ and DipZ algorithms
- observables: A lot of useful plotting routines for dependencies of HitZ on observables. Also several efficiency-like plots.
- trigger: Some studies to see the impact of L1 trigger chains on the HitZ pileup rejection.
- utils:
    - AOD_overview: A list of all dumper .h5 samples and what they contain with links to all AOD files as well.
    - evaluate_salt.sh: Several commands to evaluate training with the Salt framework.
    - generate_training_data.py: Alternative to preprocessing with umami, should not be needed. Removes NaNs. Has no implemented resampling, hence do not use for b-tagging.
    - h5_checker.py: Prints the substructure of a .h5 file.
