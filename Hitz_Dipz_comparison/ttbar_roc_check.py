############################################################################

# Required packages

import h5py
import numpy as np
import matplotlib.pyplot as plt
import itertools
import mplhep as hep
hep.style.use(hep.style.ATLAS)

############################################################################

# General parameter

num_jets = 100000

############################################################################

# Loading data

# base_path = "Dipz_20240126-T175203"

# network_1 = {
#     "Dipz" : "/eos/user/b/backes/QT/salt/salt/logs/Dipz_20240126-T175203/ckpts/epoch=113-val_loss=-2.67856__test_user.backes.36944949._000001.output.h5"
#     # "Dipz" : "/eos/user/b/backes/QT/salt/salt/logs/" + base_path + "/ckpts/mathias_ttbar_test.h5" 
# }
# network_2 = {
#     "Dipz" : "/eos/user/b/backes/QT/salt/salt/logs/" + base_path + "/ckpts/mathias_JZ0_test.h5" 
# }
# network_3 = {
#     "Dipz" : "/eos/user/b/backes/QT/salt/salt/logs/" + base_path + "/ckpts/mathias_JZ1_test.h5" 
# }
# network_4 = {
#     "Dipz" : "/eos/user/b/backes/QT/salt/salt/logs/" + base_path + "/ckpts/mathias_JZ2_test.h5" 
# }
# network_5 = {
#     "Dipz" : "/eos/user/b/backes/QT/salt/salt/logs/" + base_path + "/ckpts/mathias_JZ3_test.h5" 
# }
# network_6 = {
#     "Dipz" : "/eos/user/b/backes/QT/salt/salt/logs/" + base_path + "/ckpts/mathias_JZ4_test.h5" 
# }

# path1 = '/eos/user/b/backes/QT/preprocessing/200hits/user.backes.'

# path2 = '.e8514_e8528_s4111_s4114_r14634.tdd.trigger_hits.24_0_16.r12684_r12782.v1-200-g2a6de5b_output.h5/user.backes.'

# path3 = '._000001.output.h5'

# # test_path_1 = '/eos/user/b/backes/QT/preprocessing/Dipz_train/pp_output_test_ttbar.h5'

# test_path_1 = path1 + '601229' + path2 + '36944949' + path3

# test_path_2 = path1 + '801165' + path2 + '36944946' + path3

# test_path_3 = path1 + '801166' + path2 + '36944948' + path3

# test_path_4 = path1 + '801167' + path2 + '36944950' + path3

# test_path_5 = path1 + '801168' + path2 + '36944951' + path3

# test_path_6 = path1 + '801169' + path2 + '36944947' + path3

# reference = "ttbar_Dipz"




base_path = "Hitz_ttbar_20240129-T115654"

network_1 = {
    # "Hitz_ttbar" : "/eos/user/b/backes/QT/salt/salt/logs/" + base_path + "/ckpts/mathias_ttbar_test.h5" 
    "Hitz_ttbar" : "/eos/user/b/backes/QT/salt/salt/logs/Hitz_BIG_ttbar_small_lr_20240212-T105027/ckpts/epoch=059-val_loss=-1.13583__test_user.backes.36944949._000001.output.h5"
}
# network_2 = {
#     "Hitz_ttbar" : "/eos/user/b/backes/QT/salt/salt/logs/" + base_path + "/ckpts/mathias_JZ0_test.h5" 
# }
network_3 = {
    "Hitz_ttbar" : "/eos/user/b/backes/QT/salt/salt/logs/" + base_path + "/ckpts/mathias_JZ1_test.h5" 
}
network_4 = {
    "Hitz_ttbar" : "/eos/user/b/backes/QT/salt/salt/logs/" + base_path + "/ckpts/mathias_JZ2_test.h5" 
}
network_5 = {
    "Hitz_ttbar" : "/eos/user/b/backes/QT/salt/salt/logs/" + base_path + "/ckpts/mathias_JZ3_test.h5" 
}
network_6 = {
    "Hitz_ttbar" : "/eos/user/b/backes/QT/salt/salt/logs/" + base_path + "/ckpts/mathias_JZ4_test.h5" 
}

path1 = '/eos/user/b/backes/QT/preprocessing/200hits/user.backes.'

path2 = '.e8514_e8528_s4111_s4114_r14634.tdd.trigger_hits.24_0_16.r12684_r12782.v1-200-g2a6de5b_output.h5/user.backes.'

path3 = '._000001.output.h5'

# test_path_1 = '/eos/user/b/backes/QT/preprocessing/Hitz_train/pp_output_test_ttbar.h5'

test_path_1 = path1 + '601229' + path2 + '36944949' + path3

test_path_2 = path1 + '801165' + path2 + '36944946' + path3

test_path_3 = path1 + '801166' + path2 + '36944948' + path3

test_path_4 = path1 + '801167' + path2 + '36944950' + path3

test_path_5 = path1 + '801168' + path2 + '36944951' + path3

test_path_6 = path1 + '801169' + path2 + '36944947' + path3

reference = "ttbar_Hitz"




print("\n========================= \n" + "  " + reference + " ROC curves \n" + "========================= \n")

############################################################################

# Load test data

# Signal

with h5py.File(test_path_1, 'r') as test_f:
    jets_sig = test_f['jets'][:num_jets]
    pt_MeV_sig = jets_sig['pt']
    pt_sig = pt_MeV_sig/1000
    eventNumber_sig = jets_sig['eventNumber']
    mc_weight_sig = jets_sig['mcEventWeight']
    TruthZ_sig = jets_sig['TruthJetPVz']
for key, val in network_1.items():
    with h5py.File(val, 'r') as f:
        jets = f['jets'][:num_jets]
        jet_z_pred_sig = jets["gaussian_regression_TruthJetPVz"]
        jet_z_stddev_sig = jets["gaussian_regression_TruthJetPVz_stddev"]

# Background JZ Slices
        
# with h5py.File(test_path_2, 'r') as test_f:
#     jets_jz0 = test_f['jets'][:num_jets]
#     pt_MeV_jz0 = jets_jz0['pt']
#     pt_jz0 = pt_MeV_jz0/1000
#     eventNumber_jz0 = jets_jz0['eventNumber']
#     mc_weight_jz0 = jets_jz0['mcEventWeight']
# for key, val in network_2.items():
#     with h5py.File(val, 'r') as f:
#         jets = f['jets'][:num_jets]
#         jet_z_pred_jz0 = jets["gaussian_regression_TruthJetPVz"].reshape((-1,1))
#         jet_z_stddev_jz0 = jets["gaussian_regression_TruthJetPVz_stddev"].reshape((-1,1))

with h5py.File(test_path_3, 'r') as test_f:
    jets_jz1 = test_f['jets'][:num_jets]
    pt_MeV_jz1 = jets_jz1['pt']
    pt_jz1 = pt_MeV_jz1/1000
    eventNumber_jz1 = jets_jz1['eventNumber']
    mc_weight_jz1 = jets_jz1['mcEventWeight']
for key, val in network_3.items():
    with h5py.File(val, 'r') as f:
        jets = f['jets'][:num_jets]
        jet_z_pred_jz1 = jets["gaussian_regression_TruthJetPVz"].reshape((-1,1))
        jet_z_stddev_jz1 = jets["gaussian_regression_TruthJetPVz_stddev"].reshape((-1,1))

with h5py.File(test_path_4, 'r') as test_f:
    jets_jz2 = test_f['jets'][:num_jets]
    pt_MeV_jz2 = jets_jz2['pt']
    pt_jz2 = pt_MeV_jz2/1000
    eventNumber_jz2 = jets_jz2['eventNumber']
    mc_weight_jz2 = jets_jz2['mcEventWeight']
for key, val in network_4.items():
    with h5py.File(val, 'r') as f:
        jets = f['jets'][:num_jets]
        jet_z_pred_jz2 = jets["gaussian_regression_TruthJetPVz"].reshape((-1,1))
        jet_z_stddev_jz2 = jets["gaussian_regression_TruthJetPVz_stddev"].reshape((-1,1))

with h5py.File(test_path_5, 'r') as test_f:
    jets_jz3 = test_f['jets'][:num_jets]
    pt_MeV_jz3 = jets_jz3['pt']
    pt_jz3 = pt_MeV_jz3/1000
    eventNumber_jz3 = jets_jz3['eventNumber']
    mc_weight_jz3 = jets_jz3['mcEventWeight']
for key, val in network_5.items():
    with h5py.File(val, 'r') as f:
        jets = f['jets'][:num_jets]
        jet_z_pred_jz3 = jets["gaussian_regression_TruthJetPVz"].reshape((-1,1))
        jet_z_stddev_jz3 = jets["gaussian_regression_TruthJetPVz_stddev"].reshape((-1,1))

with h5py.File(test_path_6, 'r') as test_f:
    jets_jz4 = test_f['jets'][:num_jets]
    pt_MeV_jz4 = jets_jz4['pt']
    pt_jz4 = pt_MeV_jz4/1000
    eventNumber_jz4 = jets_jz4['eventNumber']
    mc_weight_jz4 = jets_jz4['mcEventWeight']
for key, val in network_6.items():
    with h5py.File(val, 'r') as f:
        jets = f['jets'][:num_jets]
        jet_z_pred_jz4 = jets["gaussian_regression_TruthJetPVz"].reshape((-1,1))
        jet_z_stddev_jz4 = jets["gaussian_regression_TruthJetPVz_stddev"].reshape((-1,1))

############################################################################

# Trivialise weights

# mc_weight_sig = np.ones_like(mc_weight_sig, dtype=float)
# mc_weight_jz0 = np.ones_like(mc_weight_jz0, dtype=float)
# mc_weight_jz1 = np.ones_like(mc_weight_jz1, dtype=float)
# mc_weight_jz2 = np.ones_like(mc_weight_jz2, dtype=float)
# mc_weight_jz3 = np.ones_like(mc_weight_jz3, dtype=float)
# mc_weight_jz4 = np.ones_like(mc_weight_jz4, dtype=float)
        

# mc_weight_sig = np.zeros_like(mc_weight_sig, dtype=float)
# mc_weight_jz0 = np.zeros_like(mc_weight_jz0, dtype=float)
# mc_weight_jz1 = np.zeros_like(mc_weight_jz1, dtype=float)
# mc_weight_jz2 = np.zeros_like(mc_weight_jz2, dtype=float)
# mc_weight_jz3 = np.zeros_like(mc_weight_jz3, dtype=float)
# mc_weight_jz4 = np.zeros_like(mc_weight_jz4, dtype=float)

############################################################################

# Include 4 jet condition for signal

count_throw_away = 0
sum_counter = 0 
for id in np.unique(eventNumber_sig):
    _, counts = np.unique(TruthZ_sig[eventNumber_sig == id], return_counts=True)
    if len(eventNumber_sig[eventNumber_sig == id]) < 4:
        sum_counter += 1
    if max(counts) < 4 :
        mc_weight_sig[eventNumber_sig == id] = 0.
        count_throw_away += 1

print("4-jet-condition done. Threw away ", 100*sum_counter/len(np.unique(eventNumber_sig)), "%")
print("4-jet-condition from one vertex done. Threw away ", 100*count_throw_away/len(np.unique(eventNumber_sig)), "%")

############################################################################

# Cut JZ1 at 90 GeV

for ev_num in np.unique(eventNumber_jz1):
        this_event_pt = pt_jz1[eventNumber_jz1==ev_num]
        if this_event_pt.max() > 90:
            mc_weight_jz1[eventNumber_jz1==ev_num] = 0.
            
############################################################################

# MC reweighting parameters

# cs_jz0 = 78580000000.0 *1e-12 
# filter_jz0 = 9.736785E-01 
cs_jz1 = 93901000000.0 *1e-12 
filter_jz1 = 3.513696E-02
cs_jz2 = 2582600000.0 *1e-12
filter_jz2 = 1.006522E-02 
cs_jz3 = 28528000.0 *1e-12  
filter_jz3 = 1.190844E-02 
cs_jz4 = 280140.0 *1e-12 
filter_jz4 = 1.375122E-02

total_events_sig = np.unique(eventNumber_sig).shape[0]
# total_events_jz0 = np.unique(eventNumber_jz0).shape[0]
total_events_jz1 = np.unique(eventNumber_jz1).shape[0]
total_events_jz2 = np.unique(eventNumber_jz2).shape[0]
total_events_jz3 = np.unique(eventNumber_jz3).shape[0]
total_events_jz4 = np.unique(eventNumber_jz4).shape[0]

print("Number of Signal events: ", total_events_sig)
# print("Number of JZ0 events: ", total_events_jz0)
print("Number of JZ1 events: ", total_events_jz1)
print("Number of JZ2 events: ", total_events_jz2)
print("Number of JZ3 events: ", total_events_jz3)
print("Number of JZ4 events: ", total_events_jz4)

# _, unique_events_jz0 = np.unique(eventNumber_jz0, return_index=True)
# total_events_jz0 = np.sum(mc_weight_jz0[unique_events_jz0])
_, unique_events_jz1 = np.unique(eventNumber_jz1, return_index=True)
total_events_jz1 = np.sum(mc_weight_jz1[unique_events_jz1])
_, unique_events_jz2 = np.unique(eventNumber_jz2, return_index=True)
total_events_jz2 = np.sum(mc_weight_jz2[unique_events_jz2])
_, unique_events_jz3 = np.unique(eventNumber_jz3, return_index=True)
total_events_jz3 = np.sum(mc_weight_jz3[unique_events_jz3])
_, unique_events_jz4 = np.unique(eventNumber_jz4, return_index=True)
total_events_jz4 = np.sum(mc_weight_jz4[unique_events_jz4])

# weight_jz0 = cs_jz0 * filter_jz0 / total_events_jz0 *50000
weight_jz1 = cs_jz1 * filter_jz1 / total_events_jz1 *50000
weight_jz2 = cs_jz2 * filter_jz2 / total_events_jz2 *50000
weight_jz3 = cs_jz3 * filter_jz3 / total_events_jz3 *50000
weight_jz4 = cs_jz4 * filter_jz4 / total_events_jz4 *50000

############################################################################

# Implementation of MLPL calculation

def calculate_L_max(ev_num, n, m, cur_eN, cur_pt, cur_pred, cur_std):

    this_event_pt = cur_pt[cur_eN==ev_num].argsort()
    pred = cur_pred[cur_eN==ev_num]
    std = cur_std[cur_eN==ev_num]
    pred = pred[this_event_pt[::-1]]
    pred = pred[:m]
    std = std[this_event_pt[::-1]]
    std = std[:m]

    L_max = -9999999999.
    # cur_max = 0.
    all_jets = range(m)
    for subset in itertools.combinations(all_jets, n):
        log_l = -0.5*np.log(2*np.pi)*n
        log_l += -np.sum(np.log(std[[subset]]), dtype=float)
        term1 = np.sum(np.divide(pred[[subset]], std[[subset]]**2, dtype=float), dtype=float)
        term2 = np.sum(np.divide(np.ones_like(std[[subset]],dtype=float), std[[subset]]**2, dtype=float), dtype=float)
        log_l += -np.sum( np.divide((term1/term2 - pred[[subset]])**2,(2.*std[[subset]]**2), dtype=float))
        if log_l > L_max:
            L_max = log_l
    #         cur_max = std[[subset]].max()
    # if cur_max > 47.:
    #     L_max = 9999999999.
    return L_max

def multiple_jet_roc_efficiencies_new(x, n, m, cur_eN, cur_pt, cur_pred, cur_std, cur_mc_weight):

    passed_events = 0.
    correct_events = np.zeros_like(x, dtype=float)
    cur_MLPL = np.array([])
    cur_MLPL_weights = np.array([])
    count = 0
    for ev_num in np.unique(cur_eN):
        count+=1
        jets = cur_eN[cur_eN==ev_num].shape[0]
        if m == "All":
            if jets >= n:
                passed_events += cur_mc_weight[cur_eN==ev_num][0]
                l_max = calculate_L_max(ev_num, n, jets, cur_eN, cur_pt, cur_pred, cur_std)
                cur_MLPL = np.append(cur_MLPL, l_max)
                cur_MLPL_weights = np.append(cur_MLPL_weights, cur_mc_weight[cur_eN==ev_num][0])
                correct_events[x<l_max] += cur_mc_weight[cur_eN==ev_num][0]
        else:
            if jets >= n:
                passed_events += cur_mc_weight[cur_eN==ev_num][0]
                if jets >= m: 
                    l_max = calculate_L_max(ev_num, n, m, cur_eN, cur_pt, cur_pred, cur_std)
                    correct_events[x<l_max] += cur_mc_weight[cur_eN==ev_num][0]
                else:
                    l_max = calculate_L_max(ev_num, n, jets, cur_eN, cur_pt, cur_pred, cur_std)
                    correct_events[x<l_max] += cur_mc_weight[cur_eN==ev_num][0]
    return passed_events, correct_events, cur_MLPL, cur_MLPL_weights

############################################################################

# Execution for several values of n and m

n_list = [4]
m_list = [4,5,6,"All"]

xmin = 0.9
xmax = 1
ymin = 0.9
ymax = 7e1

x = np.linspace(-50, 40, 400, dtype=float)
sig =[]
bkg =[]
tuple_entry = []

for n in n_list:
    print("n = ", n)
    for m in m_list:
        if m != "All":
            if m < n:
                continue
        print("m = ", m)

        passed_events_sig, correct_events_sig, MLPL_sig, MLPL_sig_weights = multiple_jet_roc_efficiencies_new(
            x, n, m, eventNumber_sig, pt_sig, jet_z_pred_sig, jet_z_stddev_sig, mc_weight_sig)
        
        # passed_events_jz0, correct_events_jz0, cur_MLPL_jz0, cur_MLPL_jz0_weights = multiple_jet_roc_efficiencies_new(
        #     x, n, m, eventNumber_jz0, pt_jz0, jet_z_pred_jz0, jet_z_stddev_jz0, mc_weight_jz0)

        passed_events_jz1, correct_events_jz1, cur_MLPL_jz1, cur_MLPL_jz1_weights = multiple_jet_roc_efficiencies_new(
            x, n, m, eventNumber_jz1, pt_jz1, jet_z_pred_jz1, jet_z_stddev_jz1, mc_weight_jz1)
        
        passed_events_jz2, correct_events_jz2, cur_MLPL_jz2, cur_MLPL_jz2_weights = multiple_jet_roc_efficiencies_new(
            x, n, m, eventNumber_jz2, pt_jz2, jet_z_pred_jz2, jet_z_stddev_jz2, mc_weight_jz2)
        
        passed_events_jz3, correct_events_jz3, cur_MLPL_jz3, cur_MLPL_jz3_weights = multiple_jet_roc_efficiencies_new(
            x, n, m, eventNumber_jz3, pt_jz3, jet_z_pred_jz3, jet_z_stddev_jz3, mc_weight_jz3)
       
        passed_events_jz4, correct_events_jz4, cur_MLPL_jz4, cur_MLPL_jz4_weights = multiple_jet_roc_efficiencies_new(
            x, n, m, eventNumber_jz4, pt_jz4, jet_z_pred_jz4, jet_z_stddev_jz4, mc_weight_jz4)

        eff_sig = correct_events_sig / passed_events_sig

        correct_events_bkg = (correct_events_jz1 * weight_jz1 
                   + correct_events_jz2 * weight_jz2
                   + correct_events_jz3 * weight_jz3 
                   + correct_events_jz4 * weight_jz4) 
        
        passed_events_bkg = (passed_events_jz1 * weight_jz1 
                   + passed_events_jz2 * weight_jz2 
                   + passed_events_jz3 * weight_jz3
                   + passed_events_jz4 * weight_jz4) 

        correct_events_bkg[correct_events_bkg==0] = np.nan
        eff_bkg = correct_events_bkg / passed_events_bkg

        rej_bkg = 1./eff_bkg

        sig.append(eff_sig)
        bkg.append(rej_bkg)
        tuple_entry.append((n,m))

        if m == "All":
            MLPL_bkg = np.concatenate((cur_MLPL_jz1, cur_MLPL_jz2, cur_MLPL_jz3, cur_MLPL_jz4))
            MLPL_bkg_weights = np.concatenate((cur_MLPL_jz1_weights*weight_jz1,
                                                cur_MLPL_jz2_weights*weight_jz2,
                                                cur_MLPL_jz3_weights*weight_jz3,
                                                    cur_MLPL_jz4_weights*weight_jz4))            
            mlplS, bins = np.histogram(MLPL_sig, bins=100, range=(-25,5), weights=MLPL_sig_weights, density=True)
            mlplS_uw, bins = np.histogram(MLPL_sig, bins=100, range=(-25,5), density=True)
            mlplB, bins = np.histogram(MLPL_bkg, bins=100, range=(-25,5), weights=MLPL_bkg_weights, density=True)
            mlplB_uw, bins = np.histogram(MLPL_bkg, bins=100, range=(-25,5), density=True)            

            plt.figure(figsize=(12,8))
            plt.step(bins[1:], mlplS, c='red', label="MLPL(4,All) Signal")
            # plt.step(bins[1:], mlplS_uw, c='orange', label="MLPL(4,All) Signal w/o reweighting")
            plt.step(bins[1:], mlplB, c='blue', label="MLPL(4,All) Bkg")
            plt.step(bins[1:], mlplB_uw, c='green', label="MLPL(4,All) Bkg w/o reweighting")
            plt.xticks(fontsize=20)
            plt.yticks(fontsize=20)
            plt.xlim(-24.9,4.9)
            plt.ylim(0,1)
            plt.xlabel("MLPL(4, All)", fontsize=20)
            plt.ylabel("Normalized Number of Events", fontsize=20)
            plt.legend(fontsize=20, frameon=False)
            hep.atlas.label(loc=4, data=True, label="Work in Progress", rlabel='')
            plt.savefig(reference + "/" + "Own_MLPL.png", format='png', bbox_inches='tight')
            plt.close()


x_ref = np.linspace(0.01, 1, 10000)
color = iter(plt.cm.rainbow(np.linspace(0, 1, len(tuple_entry))))

plt.figure(figsize=(12,8))
plt.rc('axes', axisbelow=True)
plt.grid(True, which="major", axis="x")
plt.grid(True, which="both", axis="y")
for l, entry in enumerate(tuple_entry):
    c = next(color)
    plt.plot(sig[l], bkg[l], marker='.', color=c, label="MLPL{}".format(entry))
plt.plot(x_ref, 1/x_ref, linestyle='--', color='black', label=r"$\epsilon_{\mathrm{sig}}= \epsilon_{\mathrm{bkg}}$")
plt.legend(fontsize=20, frameon=False)
plt.xlabel(r"Signal Efficiency $\epsilon_{\mathrm{sig}}$", fontsize=20)
plt.ylabel(r"Background Rejection $1/\epsilon_{\mathrm{bkg}}$", fontsize=20)
plt.xlim(xmin, xmax)
plt.ylim(ymin,ymax)
plt.yscale('log')
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
hep.atlas.label(loc=4, data=True, label="Work in Progress", rlabel='')
plt.savefig(reference + "/" + "Own_Roc_total.png" , format='png', bbox_inches='tight')
plt.close()

############################################################################
print("Plotting successful.")