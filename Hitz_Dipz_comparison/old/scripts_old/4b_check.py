
import h5py
import numpy as np


test_path_1 = '/eos/user/b/backes/QT/preprocessing/mo_files/user.backes.601479.e8514_e8528_s4159_s4114_r14774_r14754.tdd.trigger_dipz_new.24_0_21.r12684_r12782.v1-220-g5a6fb5b_output.h5/user.backes.37176216._000001.output.h5'
data = h5py.File(test_path_1, 'r')
jets = data['jets']
jets = jets[jets['pt'] > 120000]

jets = np.asarray(jets)
uniques = np.unique(jets["eventNumber"])

print("The number of jets in the sample is: " + str(len(jets)))
print("The number of jets in the sample with pT < 20 GeV is: " + str(len(jets[jets["pt"] < 20])))
print("The number of jets in the sample with eta > 2.5 GeV is: " + str(len(jets[jets["eta"] > 2.5])))
print("The number of events in our sample is: " + str(len(uniques)))

ptcounter = 0
counter = 0

for id in uniques:
    event_jets = jets[jets["eventNumber"] == id]
    if max(jets['pt'][jets["eventNumber"] == id]) >120000:
        ptcounter += 1
        bjets = event_jets[event_jets["HadronConeExclTruthLabelID"] == 5]
        if len(event_jets) >= 4 & len(bjets) >= 4:
            counter +=1

print("Number of events that have at least one jet with pt > 120 GeV: ", ptcounter)
print("Number of events that have at least one jet with pt > 120 GeV and at least 4 b-jets: ", counter)




