############################################################################

# Required packages

import h5py
import numpy as np
import math
from puma import Histogram, HistogramPlot, Roc, RocPlot
from puma.metrics import calc_rej
from puma.utils import get_good_colours, get_good_linestyles, logger
import matplotlib.pyplot as plt
from matplotlib import colors
import itertools

############################################################################


# DipZ or HitZ


# network_1 = {
#     "Hitz" : "/eos/user/b/backes/QT/salt/salt/logs/Hitz_20231216-T154456/ckpts/epoch=068-val_loss=-1.73468__test_ttbar.h5"
# }
# test_path_1 = '/eos/user/b/backes/QT/preprocessing/Hitz_new/pp_output_test_ttbar.h5'

# network_2 = {
#     "Hitz" : "/eos/user/b/backes/QT/salt/salt/logs/Hitz_20231216-T154456/ckpts/epoch=068-val_loss=-1.73468__test_jj.h5"
# }
# test_path_2 = '/eos/user/b/backes/QT/preprocessing/Hitz_new/pp_output_test_jj.h5'

# reference = "Hitz"


network_1 = {
    "Dipz" : "/eos/user/b/backes/QT/salt/salt/logs/Dipz_20240115-T093750/ckpts/epoch=024-val_loss=-2.20046__test_ttbar.h5" 
}
test_path_1 = '/eos/user/b/backes/QT/preprocessing/Dipz_all_JZ/pp_output_test_ttbar.h5'


network_2 = {
    "Dipz" : "/eos/user/b/backes/QT/salt/salt/logs/Dipz_20240115-T093750/ckpts/epoch=024-val_loss=-2.20046__test_jz0.h5"
}
test_path_2 = '/eos/user/b/backes/QT/preprocessing/Dipz_all_JZ/pp_output_test_jz0.h5'


network_3 = {
    "Dipz" : "/eos/user/b/backes/QT/salt/salt/logs/Dipz_20240115-T093750/ckpts/epoch=024-val_loss=-2.20046__test_jz1.h5"
}
test_path_3 = '/eos/user/b/backes/QT/preprocessing/Dipz_all_JZ/pp_output_test_jz1.h5'

network_4 = {
    "Dipz" : "/eos/user/b/backes/QT/salt/salt/logs/Dipz_20240115-T093750/ckpts/epoch=024-val_loss=-2.20046__test_jz2.h5"
}
test_path_4 = '/eos/user/b/backes/QT/preprocessing/Dipz_all_JZ/pp_output_test_jz2.h5'


network_5 = {
    "Dipz" : "/eos/user/b/backes/QT/salt/salt/logs/Dipz_20240115-T093750/ckpts/epoch=024-val_loss=-2.20046__test_jz3.h5"
}
test_path_5 = '/eos/user/b/backes/QT/preprocessing/Dipz_all_JZ/pp_output_test_jz3.h5'


network_6 = {
    "Dipz" : "/eos/user/b/backes/QT/salt/salt/logs/Dipz_20240115-T093750/ckpts/epoch=024-val_loss=-2.20046__test_jz4.h5"
}
test_path_6 = '/eos/user/b/backes/QT/preprocessing/Dipz_all_JZ/pp_output_test_jz4.h5'





reference = "Dipz"





############################################################################


# Load test data

logger.info("Load data")

# Signal

num_jets_sig = 35000

with h5py.File(test_path_1, 'r') as test_f:
    jets_sig = test_f['jets'][:num_jets_sig]
    jet_z_sig = jets_sig['TruthJetPVz']
    pt_MeV_sig = jets_sig['pt']
    pt_sig = pt_MeV_sig/1000
    eta_sig = jets_sig['eta']
    eventNumber_sig = jets_sig['eventNumber']

for key, val in network_1.items():
    with h5py.File(val, 'r') as f:
        jets = f['jets'][:num_jets_sig]
        jet_z_pred_sig = jets["gaussian_regression_TruthJetPVz"].reshape((-1,1))
        jet_z_stddev_sig = jets["gaussian_regression_TruthJetPVz_stddev"].reshape((-1,1))


# Background

num_jets_jz0 = 52000
with h5py.File(test_path_2, 'r') as test_f:
    jets_jz0 = test_f['jets'][:num_jets_jz0]
    jet_z_jz0 = jets_jz0['TruthJetPVz']
    pt_MeV_jz0 = jets_jz0['pt']
    pt_jz0 = pt_MeV_jz0/1000
    eta_jz0 = jets_jz0['eta']
    eventNumber_jz0 = jets_jz0['eventNumber']
for key, val in network_2.items():
    with h5py.File(val, 'r') as f:
        jets = f['jets'][:num_jets_jz0]
        jet_z_pred_jz0 = jets["gaussian_regression_TruthJetPVz"].reshape((-1,1))
        jet_z_stddev_jz0 = jets["gaussian_regression_TruthJetPVz_stddev"].reshape((-1,1))


num_jets_jz1 = 52000
with h5py.File(test_path_2, 'r') as test_f:
    jets_jz1 = test_f['jets'][:num_jets_jz1]
    jet_z_jz1 = jets_jz1['TruthJetPVz']
    pt_MeV_jz1 = jets_jz1['pt']
    pt_jz1 = pt_MeV_jz1/1000
    eta_jz1 = jets_jz1['eta']
    eventNumber_jz1 = jets_jz1['eventNumber']
for key, val in network_2.items():
    with h5py.File(val, 'r') as f:
        jets = f['jets'][:num_jets_jz1]
        jet_z_pred_jz1 = jets["gaussian_regression_TruthJetPVz"].reshape((-1,1))
        jet_z_stddev_jz1 = jets["gaussian_regression_TruthJetPVz_stddev"].reshape((-1,1))


num_jets_jz2 = 52000
with h5py.File(test_path_2, 'r') as test_f:
    jets_jz2 = test_f['jets'][:num_jets_jz2]
    jet_z_jz2 = jets_jz2['TruthJetPVz']
    pt_MeV_jz2 = jets_jz2['pt']
    pt_jz2 = pt_MeV_jz2/1000
    eta_jz2 = jets_jz2['eta']
    eventNumber_jz2 = jets_jz2['eventNumber']
for key, val in network_2.items():
    with h5py.File(val, 'r') as f:
        jets = f['jets'][:num_jets_jz2]
        jet_z_pred_jz2 = jets["gaussian_regression_TruthJetPVz"].reshape((-1,1))
        jet_z_stddev_jz2 = jets["gaussian_regression_TruthJetPVz_stddev"].reshape((-1,1))


num_jets_jz3 = 52000
with h5py.File(test_path_2, 'r') as test_f:
    jets_jz3 = test_f['jets'][:num_jets_jz3]
    jet_z_jz3 = jets_jz3['TruthJetPVz']
    pt_MeV_jz3 = jets_jz3['pt']
    pt_jz3 = pt_MeV_jz3/1000
    eta_jz3 = jets_jz3['eta']
    eventNumber_jz3 = jets_jz3['eventNumber']
for key, val in network_2.items():
    with h5py.File(val, 'r') as f:
        jets = f['jets'][:num_jets_jz3]
        jet_z_pred_jz3 = jets["gaussian_regression_TruthJetPVz"].reshape((-1,1))
        jet_z_stddev_jz3 = jets["gaussian_regression_TruthJetPVz_stddev"].reshape((-1,1))


num_jets_jz4 = 52000
with h5py.File(test_path_2, 'r') as test_f:
    jets_jz4 = test_f['jets'][:num_jets_jz4]
    jet_z_jz4 = jets_jz4['TruthJetPVz']
    pt_MeV_jz4 = jets_jz4['pt']
    pt_jz4 = pt_MeV_jz4/1000
    eta_jz4 = jets_jz4['eta']
    eventNumber_jz4 = jets_jz4['eventNumber']
for key, val in network_2.items():
    with h5py.File(val, 'r') as f:
        jets = f['jets'][:num_jets_jz4]
        jet_z_pred_jz4 = jets["gaussian_regression_TruthJetPVz"].reshape((-1,1))
        jet_z_stddev_jz4 = jets["gaussian_regression_TruthJetPVz_stddev"].reshape((-1,1))



############################################################################


# Signal plot

jet_z_plot = HistogramPlot(
    bins=np.linspace(-200, 200, 41),
    xlabel="$z$ Position",
    ylabel="Normalised number of jets",
    figsize=(6, 4.5),
)
jet_z_plot.add(
    Histogram(
        jet_z_sig.flatten(), label="True"
    )
)
jet_z_plot.add(
        Histogram(jet_z_pred_sig.flatten(), label=reference)
)
jet_z_plot.draw()
jet_z_plot.savefig("ROC_" + reference + "/jet_z_signal.png")


############################################################################


# Background plot

# jet_z_plot = HistogramPlot(
#     bins=np.linspace(-200, 200, 41),
#     xlabel="$z$ Position",
#     ylabel="Normalised number of jets",
#     figsize=(6, 4.5),
# )
# jet_z_plot.add(
#     Histogram(
#         jet_z_bkg.flatten(), label="True"
#     )
# )
# jet_z_plot.add(
#         Histogram(jet_z_pred_bkg.flatten(), label=reference)
# )
# jet_z_plot.draw()
# jet_z_plot.savefig("ROC_" + reference + "/jet_z_background.png")


############################################################################


# 2D Signal Plot

plt.figure(figsize=(12,8))
h = plt.hist2d(jet_z_sig, jet_z_pred_sig.flatten(), bins=30, norm=colors.LogNorm())
cb = plt.colorbar(h[3])
cb.ax.tick_params(labelsize=20)
plt.xlabel("True position $z_t$", fontsize=20)
plt.ylabel("Predicted position $z_p$", fontsize=20)
plt.xlim(-150,150)
plt.ylim(-150,150)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
x = [-150,150]
plt.plot(x,x,c='red')
plt.savefig("ROC_" + reference + "/2D_jet_z_signal.png", format='png', bbox_inches='tight')
plt.close()


############################################################################



############################################################################


# Calculate MLPL(n,m)

# def calculate_L_max(ev_num, n, m, type):
#     if type == "sig":
        
#         this_event_pt_sig = pt_sig[eventNumber_sig==ev_num].argsort()
#         pred = jet_z_pred_sig[eventNumber_sig==ev_num]
#         std = jet_z_stddev_sig[eventNumber_sig==ev_num]
#         pred = pred[this_event_pt_sig[::-1]]
#         pred = pred[:m]
#         std = std[this_event_pt_sig[::-1]]
#         std =std[:m]
#     elif type == "bkg":
#         this_event_pt_bkg = pt_bkg[eventNumber_bkg==ev_num].argsort()
#         pred = jet_z_pred_bkg[eventNumber_bkg==ev_num]
#         std = jet_z_stddev_bkg[eventNumber_bkg==ev_num]
#         pred = pred[this_event_pt_bkg[::-1]]
#         pred = pred[:m]
#         std = std[this_event_pt_bkg[::-1]]
#         std =std[:m]
#     else:
#         print("Error: Wrong type. Choose sig or bkg.")

#     L_max = -1000
#     all_jets = range(m)
#     for subset in itertools.combinations(all_jets, n):
#         f = np.vectorize(math.log)
#         log_l = -0.5*math.log(2*math.pi)*n
#         log_l += -np.sum(f(std[[subset]]))
#         term = np.sum(pred[[subset]]/std[[subset]]**2) / np.sum(1/std[[subset]]**2)
#         log_l += -np.sum(( term - pred[[subset]])**2 / (2*std[[subset]]**2))
        
#         if log_l > L_max:
#             L_max = log_l
            

#     return L_max


# def multiple_jet_roc_efficiencies(x, n, m):

#     sorted_events_sig = eventNumber_sig.argsort()
#     total_events_sig = 0.
#     correct_events_sig = np.zeros_like(x)
#     correct_vertex_sig = 0.

#     sorted_events_bkg = eventNumber_bkg.argsort()
#     total_events_bkg = 0.
#     correct_events_bkg = np.zeros_like(x)
#     correct_vertex_bkg = 0.

#     MLPL_sig = np.array([])
#     MLPL_bkg = np.array([])

#     if m == "All":
            
#         for ev_num in np.unique(eventNumber_sig[sorted_events_sig]):
#             _, counts_sig = np.unique(jet_z_sig[eventNumber_sig==ev_num], return_counts=True)
#             jets = eventNumber_sig[eventNumber_sig==ev_num].shape[0]
#             if jets >= n: # and np.max(counts_sig) >= n:
#                 total_events_sig += 1
#                 l_max = calculate_L_max(ev_num, n, jets, "sig")
#                 MLPL_sig = np.append(MLPL_sig, l_max)
#                 correct_events_sig[x<l_max] += 1
#                 if np.max(counts_sig) >= n:
#                     correct_vertex_sig += 1

#         for ev_num in np.unique(eventNumber_bkg[sorted_events_bkg]):
#             _, counts_bkg = np.unique(jet_z_bkg[eventNumber_bkg==ev_num], return_counts=True)
#             jets = eventNumber_bkg[eventNumber_bkg==ev_num].shape[0]
#             if jets >= n: # and np.max(counts_bkg) < n:
#                 total_events_bkg += 1
#                 l_max = calculate_L_max(ev_num, n, jets, "bkg")
#                 MLPL_bkg = np.append(MLPL_bkg, l_max)
#                 correct_events_bkg[x<l_max] += 1
#                 if np.max(counts_bkg) >= n:
#                     correct_vertex_bkg += 1
    
#     else:
#         for ev_num in np.unique(eventNumber_sig[sorted_events_sig]):
#             _, counts_sig = np.unique(jet_z_sig[eventNumber_sig==ev_num], return_counts=True)
#             if eventNumber_sig[eventNumber_sig==ev_num].shape[0] >= m: # and np.max(counts_sig) >= n:
#                 total_events_sig += 1
#                 l_max = calculate_L_max(ev_num, n, m, "sig")
#                 correct_events_sig[x<l_max] += 1
#                 if np.max(counts_sig) >= n:
#                     correct_vertex_sig += 1

#         for ev_num in np.unique(eventNumber_bkg[sorted_events_bkg]):
#             _, counts_bkg = np.unique(jet_z_bkg[eventNumber_bkg==ev_num], return_counts=True)
#             if eventNumber_bkg[eventNumber_bkg==ev_num].shape[0] >= m: # and np.max(counts_bkg) < n:
#                 total_events_bkg += 1
#                 l_max = calculate_L_max(ev_num, n, m, "bkg")
#                 correct_events_bkg[x<l_max] += 1
#                 if np.max(counts_bkg) >= n:
#                     correct_vertex_bkg += 1
            
#     efficiency_sig = correct_events_sig/total_events_sig

#     correct_events_bkg[correct_events_bkg==0] = np.nan
#     efficiency_bkg = correct_events_bkg/total_events_bkg
#     rejection_bkg = 1./efficiency_bkg

#     exp_sig = correct_vertex_sig/total_events_sig
#     exp_bkg = correct_vertex_bkg/total_events_bkg
#     # print("Signal correct vertex:", exp_sig)
#     # print("Background correct vertex:", exp_bkg)
    
#     return efficiency_sig, rejection_bkg, exp_sig, exp_bkg, MLPL_sig, MLPL_bkg







def calculate_L_max(ev_num, n, m, cur_eN, cur_pt, cur_pred, cur_std):


    this_event_pt = cur_pt[cur_eN==ev_num].argsort()
    pred = cur_pred[cur_eN==ev_num]
    std = cur_std[cur_eN==ev_num]
    pred = pred[this_event_pt[::-1]]
    pred = pred[:m]
    std = std[this_event_pt[::-1]]
    std =std[:m]

    
    L_max = -1000
    all_jets = range(m)
    for subset in itertools.combinations(all_jets, n):
        f = np.vectorize(math.log)
        log_l = -0.5*math.log(2*math.pi)*n
        log_l += -np.sum(f(std[[subset]]))
        term = np.sum(pred[[subset]]/std[[subset]]**2) / np.sum(1/std[[subset]]**2)
        log_l += -np.sum(( term - pred[[subset]])**2 / (2*std[[subset]]**2))
        
        if log_l > L_max:
            L_max = log_l
            
    return L_max



def multiple_jet_roc_efficiencies_new(x, n, m, cur_eN, cur_pt, cur_pred, cur_std):

    #sorted_events = cur_eN.argsort()
    total_events = 0.
    passed_events = 0.
    correct_events = np.zeros_like(x)

    cur_MLPL = np.array([])
    

    for ev_num in np.unique(cur_eN):     #(cur_eN[sorted_events])
        total_events += 1
        jets = cur_eN[cur_eN==ev_num].shape[0]

        if m == "All":
            if jets >= n: # and np.max(counts_sig) >= n:
                passed_events += 1
                l_max = calculate_L_max(ev_num, n, jets, cur_eN, cur_pt, cur_pred, cur_std)
                cur_MLPL = np.append(cur_MLPL, l_max)
                correct_events[x<l_max] += 1
        else:
            if jets >= m: # and np.max(counts_sig) >= n:
                passed_events += 1
                l_max = calculate_L_max(ev_num, n, m, cur_eN, cur_pt, cur_pred, cur_std)
                correct_events[x<l_max] += 1

    return total_events, passed_events, correct_events, cur_MLPL





x = np.linspace(-50, 40, 200)
n_list = [4]
m_list = ["All"]

sig =[]
bkg =[]
tuple_entry = []

for n in n_list:
    print("n = ", n)
    for m in m_list:
        if m != "All":
            if m < n:
                continue
        print("m = ", m)

        total_events_sig, passed_events_sig, correct_events_sig, MLPL_sig = multiple_jet_roc_efficiencies_new(
            x, n, m, eventNumber_sig, pt_sig, jet_z_pred_sig, jet_z_stddev_sig)

        total_events_jz0, passed_events_jz0, correct_events_jz0, cur_MLPL_jz0 = multiple_jet_roc_efficiencies_new(
            x, n, m, eventNumber_jz0, pt_jz0, jet_z_pred_jz0, jet_z_stddev_jz0)

        total_events_jz1, passed_events_jz1, correct_events_jz1, cur_MLPL_jz1 = multiple_jet_roc_efficiencies_new(
            x, n, m, eventNumber_jz1, pt_jz1, jet_z_pred_jz1, jet_z_stddev_jz1)
        
        total_events_jz2, passed_events_jz2, correct_events_jz2, cur_MLPL_jz2 = multiple_jet_roc_efficiencies_new(
            x, n, m, eventNumber_jz2, pt_jz2, jet_z_pred_jz2, jet_z_stddev_jz2)
        
        total_events_jz3, passed_events_jz3, correct_events_jz3, cur_MLPL_jz3 = multiple_jet_roc_efficiencies_new(
            x, n, m, eventNumber_jz3, pt_jz3, jet_z_pred_jz3, jet_z_stddev_jz3)
       
        total_events_jz4, passed_events_jz4, correct_events_jz4, cur_MLPL_jz4 = multiple_jet_roc_efficiencies_new(
            x, n, m, eventNumber_jz4, pt_jz4, jet_z_pred_jz4, jet_z_stddev_jz4)


        mc_prod_jz0 = 1.
        mc_prod_jz1 = 1.
        mc_prod_jz2 = 1.
        mc_prod_jz3 = 1.
        mc_prod_jz4 = 1.


        weight_jz0 = 50000 / passed_events_jz0 * mc_prod_jz0
        weight_jz1 = 50000 / passed_events_jz1 * mc_prod_jz1
        weight_jz2 = 50000 / passed_events_jz2 * mc_prod_jz2
        weight_jz3 = 50000 / passed_events_jz3 * mc_prod_jz3
        weight_jz4 = 50000 / passed_events_jz4 * mc_prod_jz4


        eff_sig = correct_events_sig / passed_events_sig

        correct_events_bkg = (correct_events_jz0 * weight_jz0 
                   + correct_events_jz1 * weight_jz1 
                   + correct_events_jz2 * weight_jz2 
                   + correct_events_jz3 * weight_jz3 
                   + correct_events_jz4 * weight_jz4)
        
        passed_events_bkg = (passed_events_jz0 * weight_jz0 
                   + passed_events_jz1 * weight_jz1 
                   + passed_events_jz2 * weight_jz2 
                   + passed_events_jz3 * weight_jz3 
                   + passed_events_jz4 * weight_jz4)

        correct_events_bkg[correct_events_bkg==0] = np.nan
        eff_bkg = correct_events_bkg / passed_events_bkg

        rej_bkg = 1./eff_bkg
        
        print (cur_MLPL_jz0.shape)
        print(total_events_jz0)
        print(passed_events_jz0)
        print(total_events_jz2)
        print(passed_events_jz2)
        print(total_events_jz3)
        print(passed_events_jz3)
        print(total_events_jz4)
        print(passed_events_jz4)
        MLPL_bkg = np.concatenate((cur_MLPL_jz0, cur_MLPL_jz1, cur_MLPL_jz2, cur_MLPL_jz3, cur_MLPL_jz4))
        
        x_ref = np.linspace(0.01,1,10000)
        plt.figure(figsize=(12,8))
        plt.plot(eff_sig, rej_bkg, marker='o', color='blue', label="MLPL(n={},m={})".format(n,m))
        plt.plot(x_ref, 1/x_ref, linestyle='--', color='red', label=r"$\epsilon_{\mathrm{sig}}= \epsilon_{\mathrm{bkg}}$")
        # plt.scatter(exp_sig, 1./exp_bkg, marker="s", s=100, color="red",
                    # label="$\epsilon_{\mathrm{sig}}=$"+"{:0.2f}, ".format(exp_sig) + "$1/\epsilon_{\mathrm{bkg}}=$"+"{:0.2f}".format(1./exp_bkg))
        plt.legend(fontsize=20, frameon=False)
        plt.title("ROC Curve", fontsize=20)
        plt.xlabel(r"Signal Efficiency $\epsilon_{\mathrm{sig}}$", fontsize=20)
        plt.ylabel(r"Background Rejection $1/\epsilon_{\mathrm{bkg}}$", fontsize=20)
        plt.xlim(0,1)
        plt.yscale('log')
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.grid(True, which="both")
        plt.savefig("ROC_" + reference + "/" + "Roc_n={}_m={}.png".format(n,m), format='png', bbox_inches='tight')
        plt.close()

        sig.append(eff_sig)
        bkg.append(rej_bkg)
        tuple_entry.append((n,m))


        if m == "All":
            print(MLPL_bkg.shape)
            mlplS, bins = np.histogram(MLPL_sig, bins=100, range=(-100,10))
            mlplB, bins = np.histogram(MLPL_bkg, bins=100, range=(-100,10))
            
            plt.figure(figsize=(12,8))
            plt.step(bins[1:], mlplS, c='red', label="MLPL(4,All) Signal")
            plt.step(bins[1:], mlplB, c='blue', label="MLPL(4,All) Background")
            plt.xticks(fontsize=20)
            plt.yticks(fontsize=20)
            plt.xlabel("MLPL(4, All)", fontsize=20)
            plt.ylabel("Number of Events", fontsize=20)
            plt.legend(fontsize=20, frameon=False)
            plt.savefig("MLPL_" + reference + "/" + "Roc_n={}_m={}.png".format(n,m), format='png', bbox_inches='tight')
            plt.close()




x_ref = np.linspace(0.01, 1, 10000)
color = iter(plt.cm.rainbow(np.linspace(0, 1, len(tuple_entry))))

plt.figure(figsize=(12,8))
for l, entry in enumerate(tuple_entry):
    c = next(color)
    plt.plot(sig[l], bkg[l], marker='o', color=c, label="MLPL{}".format(entry))
plt.plot(x_ref, 1/x_ref, linestyle='--', color='black', label=r"$\epsilon_{\mathrm{sig}}= \epsilon_{\mathrm{bkg}}$")
plt.legend(fontsize=20, frameon=False)
plt.title("ROC Curves", fontsize=20)
plt.xlabel(r"Signal Efficiency $\epsilon_{\mathrm{sig}}$", fontsize=20)
plt.ylabel(r"Background Rejection $1/\epsilon_{\mathrm{bkg}}$", fontsize=20)
plt.xlim(0,1)
plt.yscale('log')
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
plt.grid(True, which="both")
plt.savefig("ROC_" + reference + "/" + "Roc_total.png" , format='png', bbox_inches='tight')
plt.close()




















# x = np.linspace(-50, 40, 200)
# n_list = [4]
# m_list = [4,5,"All"]

# sig =[]
# bkg =[]
# tuple_entry = []

# for n in n_list:
#     print("n = ", n)
#     for m in m_list:
#         if m != "All":
#             if m < n:
#                 continue

#         print("m = ", m)
#         eff_sig, rej_bkg, exp_sig, exp_bkg, MLPL_sig, MLPL_bkg = multiple_jet_roc_efficiencies(x, n, m)
#         x_ref = np.linspace(0.01,1,10000)

#         plt.figure(figsize=(12,8))
#         plt.plot(eff_sig, rej_bkg, marker='o', color='blue', label="MLPL(n={},m={})".format(n,m))
#         plt.plot(x_ref, 1/x_ref, linestyle='--', color='red', label=r"$\epsilon_{\mathrm{sig}}= \epsilon_{\mathrm{bkg}}$")
#         plt.scatter(exp_sig, 1./exp_bkg, marker="s", s=100, color="red",
#                     label="$\epsilon_{\mathrm{sig}}=$"+"{:0.2f}, ".format(exp_sig) + "$1/\epsilon_{\mathrm{bkg}}=$"+"{:0.2f}".format(1./exp_bkg))
#         plt.legend(fontsize=20, frameon=False)
#         plt.title("ROC Curve", fontsize=20)
#         plt.xlabel(r"Signal Efficiency $\epsilon_{\mathrm{sig}}$", fontsize=20)
#         plt.ylabel(r"Background Rejection $1/\epsilon_{\mathrm{bkg}}$", fontsize=20)
#         plt.xlim(0,1)
#         plt.yscale('log')
#         plt.xticks(fontsize=20)
#         plt.yticks(fontsize=20)
#         plt.grid(True, which="both")
#         plt.savefig("ROC_" + reference + "/" + "Roc_n={}_m={}.png".format(n,m), format='png', bbox_inches='tight')
#         plt.close()

#         sig.append(eff_sig)
#         bkg.append(rej_bkg)
#         tuple_entry.append((n,m))


#         if m == "All":
#             print(MLPL_bkg.shape)
#             mlplS, bins = np.histogram(MLPL_sig, bins=100, range=(-100,10))
#             mlplB, bins = np.histogram(MLPL_bkg, bins=100, range=(-100,10))
            
#             plt.figure(figsize=(12,8))
#             plt.step(bins[1:], mlplS, c='red', label="MLPL(4,All) Signal")
#             plt.step(bins[1:], mlplB, c='blue', label="MLPL(4,All) Background")
#             plt.xticks(fontsize=20)
#             plt.yticks(fontsize=20)
#             plt.xlabel("MLPL(4, All)", fontsize=20)
#             plt.ylabel("Number of Events", fontsize=20)
#             plt.legend(fontsize=20, frameon=False)
#             plt.savefig("MLPL_" + reference + "/" + "Roc_n={}_m={}.png".format(n,m), format='png', bbox_inches='tight')
#             plt.close()




# x_ref = np.linspace(0.01, 1, 10000)
# color = iter(plt.cm.rainbow(np.linspace(0, 1, len(tuple_entry))))

# plt.figure(figsize=(12,8))
# for l, entry in enumerate(tuple_entry):
#     c = next(color)
#     plt.plot(sig[l], bkg[l], marker='o', color=c, label="MLPL{}".format(entry))
# plt.plot(x_ref, 1/x_ref, linestyle='--', color='black', label=r"$\epsilon_{\mathrm{sig}}= \epsilon_{\mathrm{bkg}}$")
# plt.legend(fontsize=20, frameon=False)
# plt.title("ROC Curves", fontsize=20)
# plt.xlabel(r"Signal Efficiency $\epsilon_{\mathrm{sig}}$", fontsize=20)
# plt.ylabel(r"Background Rejection $1/\epsilon_{\mathrm{bkg}}$", fontsize=20)
# plt.xlim(0,1)
# plt.yscale('log')
# plt.xticks(fontsize=20)
# plt.yticks(fontsize=20)
# plt.grid(True, which="both")
# plt.savefig("ROC_" + reference + "/" + "Roc_total" , format='png', bbox_inches='tight')
# plt.close()




############################################################################


# weights_jz0 = np.full_like(jz0_dist,norm_jz0 * jz0_eff_pres * 0.7651165653000001).tolist()
# weights_jz1 = np.full_like(jz1_dist,norm_jz1 * jz1_eff_pres * 0.0032993956809600007).tolist()
# weights_jz2 = np.full_like(jz2_dist,norm_jz2 * jz2_eff_pres * 2.6009622717000003e-05).tolist()
# weights_jz3 = np.full_like(jz3_dist,norm_jz3 * jz3_eff_pres * 3.3590422200000005e-07).tolist()
# weights_jz4 = np.full_like(jz4_dist,norm_jz4 * jz4_eff_pres * 3.8673973868e-09).tolist()
# weights_jz5 = np.full_like(jz5_dist,norm_jz5 * jz5_eff_pres * 7.7799492096e-11).tolist()

############################################################################





# eff_sig = []
# rej_bkg = []

# for x in np.linspace(-30, 0, 30):
#     print("Process x = {}".format(x))
#     e_sig, r_bkg = roc_efficiencies(x)
#     eff_sig.append(e_sig)
#     rej_bkg.append(r_bkg)



        # for k, x_val in enumerate(x):
                    # if log_max > x_val:
                        # correct_events_bkg[k] += 1


#  a=0 
#         for j in subset:
#             a += -0.5*math.log(2*math.pi) - math.log(std[j]) - ( np.sum(pred[[subset]]/std[[subset]]**2) / np.sum(1/std[[subset]]**2) - pred[j])**2 / (2*std[j]**2)
#         print(log_l)
#         print(a)

############################################################################





# Calculating the hypo for 4 jets


# def roc_efficiencies(x):

#     sorted_events_sig = eventNumber_sig.argsort()
#     total_events_sig = 0
#     correct_events_sig = 0
#     evs = 0

#     for ev_num in np.unique(eventNumber_sig[sorted_events_sig]):
#         evs += 1
#         if eventNumber_sig[eventNumber_sig==ev_num].shape[0] == 4:# and np.unique(jet_z_sig[eventNumber_sig==ev_num]).shape[0] == 1:
#             total_events_sig+=1
#             log_l = 0
#             for j in range(4):
#                 pred = jet_z_pred_sig[eventNumber_sig==ev_num]
#                 std = jet_z_stddev_sig[eventNumber_sig==ev_num]
#                 log_l += -0.5*math.log(2*math.pi) - math.log(std[j]) - ( np.sum(pred/std**2) / np.sum(1/std**2) - pred[j])**2 / (2*std[j]**2)
#             if log_l > x:
#                 correct_events_sig += 1
#     #print("Fraction of signal events:" ,total_events_sig/evs)

#     sorted_events_bkg = eventNumber_bkg.argsort()
#     total_events_bkg = 0
#     correct_events_bkg = 0

#     for ev_num in np.unique(eventNumber_bkg[sorted_events_bkg]):
#         if eventNumber_bkg[eventNumber_bkg==ev_num].shape[0] == 4:# and np.unique(jet_z_bkg[eventNumber_bkg==ev_num]).shape[0] > 1:
#             total_events_bkg+=1
#             log_l = 0
#             for j in range(4):
#                 pred = jet_z_pred_bkg[eventNumber_bkg==ev_num]
#                 std = jet_z_stddev_bkg[eventNumber_bkg==ev_num]
#                 log_l += -0.5*math.log(2*math.pi) - math.log(std[j]) - ( np.sum(pred/std**2) / np.sum(1/std**2) - pred[j])**2 / (2*std[j]**2)
#             if log_l > x:
#                 correct_events_bkg += 1

#     efficiency_sig = correct_events_sig/total_events_sig
#     if correct_events_bkg == 0:
#         rejection_bkg = np.nan
#     else:
#         rejection_bkg = 1./(correct_events_bkg/total_events_bkg)

#     return efficiency_sig, rejection_bkg
    

# eff_sig = []
# rej_bkg = []

# for x in np.linspace(-30, 0, 10):
#     print("Process x = {}".format(x))
#     e_sig, r_bkg = roc_efficiencies(x)
#     eff_sig.append(e_sig)
#     rej_bkg.append(r_bkg)

# x_ref = np.linspace(0.01,1,10000)
# y_ref = 1/x_ref

# plt.figure(figsize=(12,8))
# plt.plot(eff_sig, rej_bkg, marker='o', color='blue', label="$N_{\mathrm{jets}}=4$")
# plt.plot(x_ref, y_ref, linestyle='--', color='red', label=r"$\epsilon_{\mathrm{sig}}= \epsilon_{\mathrm{bkg}}$")
# plt.legend(fontsize=20, frameon=False)
# plt.title("ROC Curve for four jets", fontsize=20)
# plt.xlabel(r"Signal Efficiency $\epsilon_{\mathrm{sig}}$", fontsize=20)
# plt.ylabel(r"Background Rejection $1/\epsilon_{\mathrm{bkg}}$", fontsize=20)
# plt.xlim(0,1)
# plt.yscale('log')
# plt.xticks(fontsize=20)
# plt.yticks(fontsize=20)
# plt.grid(True, which="both")
# plt.savefig("ROC_" + reference + "/" + "Roc_4.png", format='png', bbox_inches='tight')
# plt.close()


# plt.figure(figsize=(12,8))
# plt.plot(np.array(eff_sig)[np.array(eff_sig)>0.9], np.array(rej_bkg)[np.array(eff_sig)>0.9], marker='o', color='blue', label="$N_{\mathrm{jets}}=4$")
# plt.plot(x_ref[x_ref>0.9], y_ref[x_ref>0.9], linestyle='--', color='red', label=r"$\epsilon_{\mathrm{sig}}= \epsilon_{\mathrm{bkg}}$")
# plt.legend(fontsize=20, frameon=False)
# plt.title("ROC Curve for Four Jets", fontsize=20)
# plt.xlabel(r"Signal Efficiency $\epsilon_{\mathrm{sig}}$", fontsize=20)
# plt.ylabel(r"Background Rejection $1/\epsilon_{\mathrm{bkg}}$", fontsize=20)
# plt.xlim(0.9,1)
# plt.yscale('log')
# plt.xticks(fontsize=20)
# plt.yticks(fontsize=20)
# plt.grid(True, which="both")
# plt.savefig("ROC_" + reference + "/" + "Roc_4_zoomed.png", format='png', bbox_inches='tight')
# plt.close()


############################################################################








# # Some single events

# sorted_events = eventNumber.argsort()


# def Gauss(x, mu, sigma):
#     return 1/np.sqrt(2*np.pi*sigma**2) * np.exp(-(x-mu)**2/(2*sigma**2))

# i=2

# while i<12:

#     for ev_num in np.unique(eventNumber[sorted_events]):
        
#         if eventNumber[eventNumber==ev_num].shape[0] == i and np.unique(jet_z[eventNumber==ev_num]).shape[0] > 1:

#             color = iter(plt.cm.rainbow(np.linspace(0, 1, i)))

#             plt.figure(figsize=(12,8))
#             x = np.linspace(-150,150, 100000)
#             for j in range(i):
#                 c = next(color)
#                 y = Gauss(x, jet_z_hitz[eventNumber==ev_num][j,0], jet_z_hitz[eventNumber==ev_num][j,1])
#                 plt.plot(x[y>0.0001], y[y>0.0001]/np.max(y),
#                         label = "$z_t=${:0.3f}, $z_p=${:0.3f}, $\sigma_p=${:0.3f}".format(
#                             jet_z[eventNumber==ev_num][j],jet_z_hitz[eventNumber==ev_num][j,0], jet_z_hitz[eventNumber==ev_num][j,1],2),
#                         color=c)
#                 plt.vlines(jet_z[eventNumber==ev_num][j], 0, 1, color=c, ls = '--', lw=2-j*0.2)

#             plt.xlabel("$z$ Position", fontsize=20)
#             plt.ylabel("Single event distributions", fontsize=20)
#             plt.ylim(0,1.8)
#             plt.xticks(fontsize=20)
#             plt.yticks(fontsize=20)
#             first_legend = plt.legend(loc="upper center", fontsize=15, ncol=2, frameon=False)
#             plt.gca().add_artist(first_legend)
#             # line1, = plt.plot([], label="$z_t$", linestyle='--', linewidth=2, c='black')
#             # line2, = plt.plot([], label="$G(z_p,\\sigma_p)$", linewidth=2, c='black')
#             # plt.legend(handles=[line1, line2], loc=(0.35,0.6), fontsize=15, ncol=2, frameon=False)
#             plt.savefig("Event_" + reference + "/{}_jet_event.png".format(i), format='png', bbox_inches='tight')
#             plt.close()
#             break
#     i += 1
    






# x1=0
# x2=0
# x3=0
# x4=0
# x5=0
# x6=0
# x7=0
# x8=0
# x9=0
# x10=0


# for ev_num in np.unique(eventNumber[sorted_events]):
#     if eventNumber[eventNumber==ev_num].shape[0]==1:
#         x1+=1
#     if eventNumber[eventNumber==ev_num].shape[0]==2:
#         x2+=1
#     if eventNumber[eventNumber==ev_num].shape[0]==3:
#         x3+=1
#     if eventNumber[eventNumber==ev_num].shape[0]==4:
#         x4+=1
#     if eventNumber[eventNumber==ev_num].shape[0]==5:
#         x5+=1
#         # print(eventNumber[eventNumber==ev_num])
#     if eventNumber[eventNumber==ev_num].shape[0]==6:
#         x6+=1
#     if eventNumber[eventNumber==ev_num].shape[0]==7:
#         x7+=1
#     if eventNumber[eventNumber==ev_num].shape[0]==8:
#         x8+=1
#     if eventNumber[eventNumber==ev_num].shape[0]==9:
#         x9+=1
#     if eventNumber[eventNumber==ev_num].shape[0]==10:
#         x10+=1

# print(x1/np.unique(eventNumber[sorted_events]).shape[0])
# print(x2/np.unique(eventNumber[sorted_events]).shape[0])
# print(x3/np.unique(eventNumber[sorted_events]).shape[0])
# print(x4/np.unique(eventNumber[sorted_events]).shape[0])
# print(x5/np.unique(eventNumber[sorted_events]).shape[0])
# print(x6/np.unique(eventNumber[sorted_events]).shape[0])
# print(x7/np.unique(eventNumber[sorted_events]).shape[0])
# print(x8/np.unique(eventNumber[sorted_events]).shape[0])
# print(x9/np.unique(eventNumber[sorted_events]).shape[0])
# print(x10/np.unique(eventNumber[sorted_events]).shape[0])
# print((x1+x2+x3+x4+x5+x6+x7+x8+x9+x10)/np.unique(eventNumber[sorted_events]).shape[0])


############################################################################
print("Plotting successful.")
