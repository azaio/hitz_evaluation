########################################################################################################################################################

# Main evaluation framework for HitZ

########################################################################################################################################################

# Required packages

import h5py
import numpy as np
import matplotlib.pyplot as plt
import itertools
from matplotlib import colors
import glob
import mplhep as hep
hep.style.use(hep.style.ATLAS)

print("\n=========================== \n" + " eta dependencies \n" + "=========================== \n")

########################################################################################################################################################

# Load Data


base_path = '/eos/user/b/backes/QT/preprocessing/'
network_base = '/eos/user/b/backes/QT/salt/salt/logs/' + "HitZ_btagging_with_z_128_weight_20240325-T180623" + "/ckpts/epoch=*." # with pt resampling
# network_base = '/eos/user/b/backes/QT/salt/salt/logs/' + "HitZ_btagging_20240322-T174710" + "/ckpts/epoch=*." # without pt resampling

test_path_1 = glob.glob(base_path + "L1_btagging/" + "pp_output_test_ttbar.h5")[0]
network_path_1 = glob.glob(network_base +"*_test_ttbar.h5")[0]

num_jets = -1 #10000

with h5py.File(test_path_1, 'r') as test_f:
    jets_sig = test_f['jets'][:num_jets]
    eventNumber_sig = jets_sig['eventNumber']
    mc_weight_sig = jets_sig['mcEventWeight']
    tag_truth = jets_sig['HadronConeExclTruthLabelID']
    pt_MeV_sig = jets_sig['pt']
    pt_sig = pt_MeV_sig/1000
    eta_sig = jets_sig["eta"]
with h5py.File(network_path_1, 'r') as f:
    jets = f['jets'][:num_jets]
    # pb = jets["HitZ_btagging_pb"]
    # pc = jets["HitZ_btagging_pc"]
    # pu = jets["HitZ_btagging_pu"]
    pb = jets["HitZ_btagging_with_z_128_weight_pb"]
    pc = jets["HitZ_btagging_with_z_128_weight_pc"]
    pu = jets["HitZ_btagging_with_z_128_weight_pu"]

mc_weight_sig=mc_weight_sig/1000

# Split test data and calculate discriminant Db

def calculate_Db(pb,pc,pu,fc=0.07):
    Db = np.log(pb/(fc*pc+(1-fc)*pu))
    return Db

pb_sig = pb[tag_truth==5]
pc_sig = pc[tag_truth==5]
pu_sig = pu[tag_truth==5]

pb_c = pb[tag_truth==4]
pc_c = pc[tag_truth==4]
pu_c = pu[tag_truth==4]

pb_u = pb[tag_truth==0]
pc_u = pc[tag_truth==0]
pu_u = pu[tag_truth==0]

Db_sig = calculate_Db(pb_sig,pc_sig,pu_sig)
Db_c = calculate_Db(pb_c,pc_c,pu_c)
Db_u = calculate_Db(pb_u,pc_u,pu_u)

eta_b = eta_sig[tag_truth==5]
eta_c = eta_sig[tag_truth==4]
eta_u = eta_sig[tag_truth==0]

mc_weight_b = mc_weight_sig[tag_truth==5]
mc_weight_c = mc_weight_sig[tag_truth==4]
mc_weight_u = mc_weight_sig[tag_truth==0]



def eta_plots(xarr, eta_atm, Db_atm, mc_weight_atm, label):

    for xval in xarr:

        b_sig_efficiency = np.sum(mc_weight_b[Db_sig>xval])/np.sum(mc_weight_b)
        c_bkg_rejection = np.sum(mc_weight_c)/np.sum(mc_weight_c[Db_c>xval])
        u_bkg_rejection = np.sum(mc_weight_u)/np.sum(mc_weight_u[Db_u>xval])

        bin_min = -3
        bin_max = 3
        bin_number = 30

        eta_full, bins = np.histogram(eta_atm, bins=bin_number, range=(bin_min,bin_max), weights=mc_weight_atm)
        eta_cut, bins = np.histogram(eta_atm[Db_atm>xval], bins=bin_number, range=(bin_min,bin_max), weights=mc_weight_atm[Db_atm > xval])
        
        fig, axs = plt.subplots(2,1,figsize=(12,8), gridspec_kw={'height_ratios': [4, 1], 'hspace':0}, sharex=False, tight_layout=True)
        hep.atlas.label(ax = axs[0], loc=4, data=True, label="Work in Progress", rlabel='')

        axs[0].step([],[],c="white", label="WP = {}".format(xval))
        axs[0].step([],[],c="white", label=r"$\varepsilon_{sig}=$"+"{:0.2f}%".format(b_sig_efficiency*100))
        axs[0].step([],[],c="white", label=r"$1/\varepsilon_{bkg}$(c-jets)="+"{:0.2f}".format(c_bkg_rejection))
        axs[0].step([],[],c="white", label=r"$1/\varepsilon_{bkg}$(light jets)="+"{:0.2f}".format(u_bkg_rejection))
        axs[0].step(bins[1:], eta_full, c='red', label="Full $p_T$ of "+label)
        axs[0].step(bins[1:], eta_cut, c='blue', label="$p_T$ with $D_b>$WP")

        eta_full[eta_full==0] = np.nan
        ratio = eta_cut/eta_full

        axs[0].set_ylabel("Number of Events", fontsize=20)
        axs[0].legend(frameon=False, fontsize= 20)
        axs[0].set_xlim(bin_min,bin_max)
        axs[0].set_ylim(0.00001,)
        axs[0].set_xticks([],[],fontsize=20)
        axs[0].yaxis.set_tick_params(labelsize=20)

        axs[1].step((bins[1:]+bins[:bin_number])/2, ratio, c='black', where='mid')
        axs[1].set_ylim([0.,1.1])
        axs[1].yaxis.set_tick_params(labelsize=20)
        axs[1].set_xticks(np.linspace(bin_min,bin_max,7), np.linspace(bin_min,bin_max,7, dtype=int), fontsize=20)
        axs[1].set_xlabel("Leading $p_T$", fontsize=20)
        axs[1].set_ylabel("Ratio", fontsize=20)
        axs[1].grid(True, which='both', axis='y')
        axs[1].set_xlim(bin_min,bin_max)

        plt.savefig("Results_eta/WP_{}_eta_dependency_".format(xval)+label+".png", format='png', bbox_inches='tight')
        plt.close()

print("Start Plotting.")

xarr = [-5,-4,-3.5,-3,-2,-1,0]
eta_plots(xarr, eta_b, Db_sig, mc_weight_b, "b")
eta_plots(xarr, eta_c, Db_c, mc_weight_c, "c")
eta_plots(xarr, eta_u, Db_u, mc_weight_u, "u")


########################################################################################################################################################

# Plot Db and ROC curves

bin_min = -5
bin_max = 5

db_sig_plot, bins = np.histogram(Db_sig, bins=50, range=(bin_min, bin_max), density=True)
db_c_plot, bins = np.histogram(Db_c, bins=50, range=(bin_min, bin_max), density=True)
db_u_plot, bins = np.histogram(Db_u, bins=50, range=(bin_min, bin_max), density=True)
         
plt.figure(figsize=(12,8))
plt.step(bins[1:], db_sig_plot, c='red', label="$D_b$(bjets))")
plt.step(bins[1:], db_c_plot, c='orange', label="$D_b$(cjets)")
plt.step(bins[1:], db_u_plot, c='blue', label="$D_b$(ujets)")
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
plt.ylim(0,1)
plt.xlim(bin_min,bin_max)
plt.xlabel("$D_b$", fontsize=20)
plt.ylabel("Normalized Number of Jets", fontsize=20)
plt.legend(fontsize=20, frameon=False)
hep.atlas.label(loc=0, data=True, label="Work in Progress", rlabel='')
plt.savefig("Results_eta/Db.png", format='png', bbox_inches='tight')
plt.close()

x = np.linspace(bin_min, bin_max, 10000, dtype = float)
b_eff = np.array([])
c_eff = np.array([])
u_eff = np.array([])

for x_val in x:
    b_eff = np.append(b_eff, Db_sig[Db_sig>x_val].shape[0]/Db_sig.shape[0])
    c_eff = np.append(c_eff, Db_c[Db_c>x_val].shape[0]/Db_c.shape[0])
    u_eff = np.append(u_eff, Db_u[Db_u>x_val].shape[0]/Db_u.shape[0])
      
x_ref = np.linspace(0.01, 1, 100000)

plt.figure(figsize=(12,8))
plt.rc('axes', axisbelow=True)
plt.grid(True, which="major", axis="x")
plt.grid(True, which="both", axis="y")

plt.plot(b_eff, 1/c_eff, color='blue', label="c-jet rejection")
plt.plot(b_eff, 1/u_eff, color='green', label="light jet rejection")
plt.plot(x_ref, 1/x_ref, linestyle='--', color='black', label=r"$\epsilon_{\mathrm{sig}}= \epsilon_{\mathrm{bkg}}$")
plt.legend(fontsize=20, frameon=False)
plt.xlabel(r"b-Tag Efficiency $\epsilon_{\mathrm{sig}}$", fontsize=20)
plt.ylabel(r"Background Rejection $1/\epsilon_{\mathrm{bkg}}$", fontsize=20)
plt.xlim(0.6, 1)
plt.ylim(0.9,2e1)
plt.yscale('log')
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
hep.atlas.label(loc=4, data=True, label="Work in Progress", rlabel='')
plt.savefig("Results_eta/btag_Roc_curve.png" , format='png', bbox_inches='tight')
plt.close()


########################################################################################################################################################

print("Evaluation successful.")